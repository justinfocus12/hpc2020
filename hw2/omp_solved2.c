/******************************************************************************
* FILE: omp_solved2.c
* DESCRIPTION:
*   Another OpenMP program with a bug. 
* AUTHOR: Blaise Barney 
* LAST REVISED: 04/06/05 
******************************************************************************/
/*
 * BUG: The variables tid, i, and total are inappropriately shared. Furthermore, the inner parallel loop allows multiple threads to modify total at once, leading to a race condition. Each run of the exectable produces a different result.
 * FIX: the variables tid, i and total should be private to each thread. I moved these declarations to inside the parallel region. (I could also have made them private explicitly in the parallel construct.) I also made the inner parallel for loop a reduction.
*/
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) 
{

/*** Spawn parallel region ***/
#pragma omp parallel num_threads(8)
  {
  int i, tid, nthreads;
  float total;
  /* Obtain thread number */
  tid = omp_get_thread_num();
  /* Only master thread does this */
  if (tid == 0) {
    nthreads = omp_get_num_threads();
    printf("Number of threads = %d\n", nthreads);
    }
  printf("Thread %d is starting...\n",tid);

  #pragma omp barrier

  /* do some work */
  total = 0.0;
  //Bug: to spawn subprocesses to calculate total, we need a parallel for loop (not a bunch of threads doing their own thing) and a reduction over total. 
  #pragma omp parallel for schedule(dynamic,10) reduction(+:total) num_threads(6)
  for (i=0; i<1000000; i++){ 
     total = total + i*1.0;
     int subtid = omp_get_thread_num();
     int num_subthreads = omp_get_num_threads();
     if (i == 0) printf("\t From subthread %d, there are %d subthreads\n", subtid,num_subthreads);
  }
  printf ("Thread %d is done! Total= %e\n",tid,total);

  } /*** End of parallel region ***/
}
