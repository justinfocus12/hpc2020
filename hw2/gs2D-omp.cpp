#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <omp.h>
#include "utils.h"

//Column-major order is our convention here

double compute_residual(double* u, double* f, int n){
    double ressq=0.0,locres=0; //squared residual and local residual
    double h2 = 1.0/pow(n+1,2);
    int n2 = pow(n,2);
    int i,j,k;
    for (k=0; k<n2; k++){
        j = k / n;
        i = k - j*n;
        // Compute the residual of the old solution
        locres = -4*u[k];
        if (i<n-1) locres += u[k+1];
        if (i>0) locres += u[k-1];
        if (j<n-1) locres += u[k+n];
        if (j>0) locres += u[k-n];
        locres *= 1.0/h2;
        locres += f[k];
        ressq += pow(locres,2);
    }
    return sqrt(ressq);
}

void gs_iteration(double* u, double* f, int n, int maxiter, int nthreads, double* residual, int num_residuals, bool print_iters){
    //Perform the Gauss-Seidel update, using not matrices but the explicit expression
    double h2 = 1.0/((n+1)*(n+1)); //pow(n+1,2);
    int n2 = n*n;
    int iter;
    int res_idx = 0;
    //printf("Iteration      Residual\n");
    //printf("%9d %16f\n", 0, compute_residual(u,f,n));
    // Count the red and black nodes. Red is upper left.
    int numblack = n2/2; //Different depending on even or odd n
    int numred = n2 - numblack; 
    // Compute the map from red and black indices to total indices
    int red_idx[numred];
    int black_idx[numblack];
    int k_pre=0, kred_pre=0, kblack_pre=0; //Differentiate from the kred and kblack inside the parallel section 
    for (int i=0; i<n; i++){
        for (int j=0; j<n; j++){
            k_pre = i + j*n;
            if ((i+j) % 2 == 0){
                red_idx[kred_pre] = k_pre;
                kred_pre++;
            } else {
                black_idx[kblack_pre] = k_pre;
                kblack_pre++;
            }
        }
    }
    //printf("Iteration      Residual\n");
    for (iter=0; iter<maxiter; iter++){
        // Red parallel loop
        #pragma omp parallel for num_threads(nthreads)
        for (int kred=0; kred<numred; kred++){
            int k = red_idx[kred];
            int tid = omp_get_thread_num();
            int j = k / n;
            int i = k - j*n;
            //printf("Starting thread %d\n", tid);
            //if (iter == 0 && kred == 0){ 
            //    printf("From thread %d of red loop: there are %d threads\n", tid, omp_get_num_threads());
            //}
            // Compute the update
            u[k] = h2*f[k];
            if (i<n-1) u[k] += u[k+1];
            if (i>0) u[k] += u[k-1];
            if (j<n-1) u[k] += u[k+n];
            if (j>0) u[k] += u[k-n];
            u[k] *= 0.25;
        }
        // Black parallel loop
        #pragma omp parallel for num_threads(nthreads)
        for (int kblack=0; kblack<numblack; kblack++){
            int k = black_idx[kblack];
            int tid = omp_get_thread_num();
            int j = k / n;
            int i = k - j*n;
            //printf("Starting thread %d\n", tid);
            //if (iter == 0 && kblack == 0){ 
            //    printf("From thread %d of black loop: there are %d threads\n", tid, omp_get_num_threads());
            //}
            // Compute the update
            u[k] = h2*f[k];
            if (i<n-1) u[k] += u[k+1];
            if (i>0) u[k] += u[k-1];
            if (j<n-1) u[k] += u[k+n];
            if (j>0) u[k] += u[k-n];
            u[k] *= 0.25;
        }
        if (print_iters && iter % (maxiter/num_residuals) == (maxiter/num_residuals-1)){
            residual[res_idx] = compute_residual(u,f,n);
            res_idx++;
            //printf("%9d %16f\n", iter, compute_residual(u,f,n));
        }
    }
    residual[num_residuals-1] = compute_residual(u,f,n);
}

void time_gs(int N, int nthreads, int maxiter, double* time, double* residual, int num_residuals, bool print_iters){
    // Run Gauss-Seidel iteration 
    int n = N-1; //Number of interior grid points along an edge
    int n2 = n*n; 
    double* u = (double*) malloc(n2 * sizeof(double)); //Solution vector (flattened)
    double* f = (double*) malloc(n2 * sizeof(double)); //Right hand side
    int k;
    for (k=0; k<n2; k++){
        u[k] = 0.0;
        f[k] = 1.0;
    }
    Timer t;
    t.tic();
    gs_iteration(u, f, n, maxiter, nthreads, residual, num_residuals, print_iters);
    *time = t.toc();
    //printf("Time = %10f\n",duration);
    free(u);
    free(f);
}

void display_gs(){
    int maxiter = 1000;
    int N = 100;
    int nthreads = 10;
    double time;
    int num_residuals = 10;
    double residual[num_residuals];
    time_gs(N, nthreads, maxiter, &time, residual, num_residuals, 1);
}

int main(int argc, char** argv){
    //display_gs();
    int maxiter = 5000;
    int Nmax = 1000;
    int nthreads_max = 10;
    int N, nthreads;
    double time;
    int num_residuals = 10;
    double residual[num_residuals]; // These will be computed by the algorithm
    // Run through several cases and print out the results in a table
    printf("N     nthreads       time    residuals (every %d iterations)\n", maxiter/num_residuals);
    for (N = Nmax/10; N <= Nmax; N += Nmax/10){
        for (nthreads = 1; nthreads <= nthreads_max; nthreads += 4){
            printf("%3d %9d %10f ", N, nthreads, time);
            time_gs(N, nthreads, maxiter, &time, residual, num_residuals, 1);
            for (int r=0; r<num_residuals; r++){
                printf("  %10f  ", residual[r]);
            }
            printf("\n");
        }
    }
    return 0;   
}
    


