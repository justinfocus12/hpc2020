/******************************************************************************
* FILE: omp_solved4.c
* DESCRIPTION:
*   This very simple program causes a segmentation fault.
* AUTHOR: Blaise Barney  01/09/04
* LAST REVISED: 04/06/05
******************************************************************************/

/*
 * BUG: the 2D stack-allocated array a is only allocated once outside the parallel region, and making it a shared variable wreaks havoc when multiple threads try to work with it. With the help of Valgrind, which alerted me to unexpected switching between stacks of different threads, I determined that a had to be declared inside the parallel region. 
 * Furthermore, declaring it on the stack did not fix the problem, whereas dynamically allocating from the heap fixed the problem. I do not understand the detailed difference between these cases, but empirically this works.
 *
 *
 */

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#define N 1048 

int main (int argc, char *argv[]) 
{
int nthreads, tid, i, j;
//double a[N][N];

/* Fork a team of threads with explicit variable scoping */
#pragma omp parallel shared(nthreads) private(i,j,tid) //,a) //num_threads(1)
  {
  //double a[N][N];
  double* a[N];
  for (int n=0; n<N; n++){
     a[n] = (double*) malloc(N * sizeof(double));
  }

  /* Obtain/print thread info */
  tid = omp_get_thread_num();
  if (tid == 0) 
    {
    nthreads = omp_get_num_threads();
    printf("Number of threads = %d\n", nthreads);
    }
  printf("Thread %d starting...\n", tid);

  /* Each thread works on its own private copy of the array */
  for (i=0; i<N; i++){
    for (j=0; j<N; j++){
      a[i][j] = tid + i + j;
    }
  }
  /* For confirmation */
  printf("Thread %d done. Last element= %f\n",tid,a[N-1][N-1]);
  for (int n=0; n<N; n++){
      free(a[n]);
  }
  }  /* All threads join master thread and disband */
}
