#include <stdio.h>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <iomanip>
#include "matrix_solvers.h"
//#include "utils.h"

void print_matrix(double *L, int nrows, int ncols){
    //Print out the flattened nrowsxncols matrix L in a formatted way
    for (int i=0; i<nrows; i++){
        for (int j=0; j<ncols; j++){
            //printf("%0.3f    ", L[i*ncols + j]);
            std::cout << std::setprecision(4) << std::setw(10) << L[i*ncols + j] << " ";
        }
        //printf("\n");
        std::cout << std::endl;
    }
}

void print_array(double *x, int n){
    //Print out an n-component vector
    printf("      ");
    for (int i=0; i<n; i++){
        //printf("%0.3f    ", x[i]);
        std::cout << std::setprecision(4) << std::setw(10) << x[i] << " ";
    }
    printf("\n");
}

void ij2k(int m, int n, int *i, int *j, int *k){
    //Given a two-dimensional index in an mxn array, convert to a flat index k
    //Assume row-major ordering
    *k = n*(*i) + *j;
}

void k2ij(int m, int n, int *i, int *j, int *k){
    //Inverse of ij2k
    *i = *k / n;
    *j = *k - n*(*i);
}

void test_index_conversions(){
    int m=3, n=4;
    int i,j,k;
    double *A = (double*) malloc(m*n*sizeof(double));
    for (k=0; k<m*n; k++){
        A[k] = k;
        k2ij(m,n,&i,&j,&k);
        printf("k=%d, i=%d, j=%d, A[i,j]=%f\n", k,i,j,A[k]);
    }   
    for (i=0; i<m; i++){
        for (j=0; j<n; j++){
            ij2k(m,n,&i,&j,&k);
            printf("k=%d, i=%d, j=%d, A[i,j]=%f\n", k,i,j,A[k]);
        }
    }
}


void jacobi_iteration(double *A, double *x, double *b, int n, int maxiter, double rel_err_tol, int *iters_needed, double *final_residual){
    //Solve the linear equation Ax=b with Jacobi iteration
    //Start from whatever initial guess is passed as a pointer, 
    //which will probably be zeros
    //Assume the diagonals of A are not zero.
    double *xold = (double*) malloc(n * sizeof(double));
    double residual,initial_residual; //Backward error 
    initial_residual = 0.0;
    double Axi; //i'th component of Ax, which gets built up into residual
    int i,j,k;
    double off_diag_sum;
    bool stop = 0;
    int it = 0;
    while (!stop){
        it++;
        for (i=0; i<n; i++)
            xold[i] = x[i];
        k = 0; //flat index in A
        for (i=0; i<n; i++){
            off_diag_sum = 0.0;
            for (j=0; j<n; j++){
                if (j != i){
                    off_diag_sum += A[k]*xold[j];
                }
                k++;
            }
            x[i] = 1.0/A[i*n+i] * (b[i] - off_diag_sum);
        }
        //Compute the backward error
        k = 0;
        residual = 0.0;
        for (i=0; i<n; i++){
            Axi = 0.0;
            for (j=0; j<n; j++){
                Axi += A[k]*x[j];
                k++;
            }
            residual += pow(Axi - b[i], 2);
        }
        residual = sqrt(residual); 
        if (it == 1){
            initial_residual = residual;
        }
        if (it % 1 == 0){  
            printf("%d              ", it);
            //print_array(x,n);
            printf("%0.15f\n", residual);
        }
        // See if the residual is below the tolerance
        stop = stop || (it >= maxiter) || (residual < initial_residual * rel_err_tol);
    }
    *iters_needed = it;
    *final_residual = residual;
    free(xold);
}

void gauss_seidel_iteration(double *A, double *x, double *b, int n, int maxiter, double rel_err_tol, int *iters_needed, double *final_residual){
    //Solve the linear equation Ax=b with Jacobi iteration
    //Start from whatever initial guess is passed as a pointer, 
    //which will probably be zeros
    //Assume the diagonals of A are not zero.
    double residual; //Backward error 
    double initial_residual = 0.0; 
    double Axi; //i'th component of Ax, which gets built up into residual
    int i,j,k;
    double off_diag_sum;
    bool stop = 0;
    int it = 0;
    while (!stop){
        it++;
        k = 0; //flat index in A
        for (i=0; i<n; i++){
            off_diag_sum = 0.0;
            for (j=0; j<n; j++){
                if (j != i){
                    off_diag_sum += A[k]*x[j];
                }
                k++;
            }
            x[i] = 1.0/A[i*n+i] * (b[i] - off_diag_sum);
        }
        //Compute the backward error
        k = 0;
        residual = 0.0;
        for (i=0; i<n; i++){
            Axi = 0.0;
            for (j=0; j<n; j++){
                Axi += A[k]*x[j];
                k++;
            }
            residual += pow(Axi - b[i], 2);
        }
        residual = sqrt(residual); 
        if (it == 1){
            initial_residual = residual; 
        }
        if (it % 1 == 0){  
            printf("%d              ", it);
            //print_array(x,n);
            printf("%0.15f\n", residual);
        }
        stop = stop || (it >= maxiter) || (residual < initial_residual * rel_err_tol);
    }
    *iters_needed = it;
    *final_residual = residual;
}

void mat_vec_mul(double *A, double *x, double *b, int n){
    //do a matrix-vector multiplication
    int k=0;
    for (int i=0; i<n; i++){
        for (int j=0; j<n; j++){
            b[i] += A[k]*x[j];
            k++;
        }
    }
}

int testing_main(){ //int argc, char **argv){
    //First test the index converters
    //test_index_conversions();
    int n = 5;
    int maxiter = 20;
    double *A = (double*) malloc(pow(n,2) * sizeof(double));
    double *b = (double*) malloc(n * sizeof(double));
    double *x = (double*) malloc(n * sizeof(double));
    double *xtrue = (double*) malloc(n * sizeof(double));
    int iters_needed = 0;
    double final_error; 
    double rel_err_tol = 1.0e-6;
    double rowsum = 0.0; //Used to ensure A is SDD
    int random_sign; //Used for generating the diagonal entries of A
    srand(time(NULL)); //set a random seed
    //Initialize A and b. Make sure A is strictly diagonally dominant
    int k = 0;
    for (int i=0; i<n; i++){
        rowsum = 0.0;
        for (int j=0; j<n; j++){
            if (j != i){
                A[k] = 2*((double) rand()/RAND_MAX) - 1;
                rowsum += fabs(A[k]);
            }
            k++;
        }
        random_sign = 2*(rand() % 2) - 1;
        A[i*n+i] = rowsum*1.1*random_sign;
        xtrue[i] = (double) rand()/RAND_MAX;
    }
    //Print A
    printf("A matrix:\n");
    print_matrix(A,n,n);
    //Print out what the answer should be
    printf("True x: \n");
    print_array(xtrue, n);
    mat_vec_mul(A, xtrue, b, n);
    jacobi_iteration(A, x, b, n, maxiter, rel_err_tol, &iters_needed, &final_error);
    printf("Jacobi-iterated x: \n");
    print_array(x, n);
    free(A);
    free(b);
    free(x);
    free(xtrue);
    return 0;
}

/*
int main(int argc, char** argv){
    int tm = testing_main();
    return tm;
}
*/
