// g++ -fopenmp -O3 -march=native MMult1.cpp && ./a.out
#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <iomanip>

#include <stdio.h>
#include <math.h>
#include <omp.h>
#include "utils.h"

#define BLOCK_SIZE 16 
//Cache size on Midway = 35840 KB. From vim -R /proc/cpuinfo

void print_matrix(double *L, int nrows, int ncols){
    //Print out the flattened (nrows)x(ncols) matrix L in a formatted way
    for (int i=0; i<nrows; i++){
        for (int j=0; j<ncols; j++){
            std::cout << std::setprecision(4) << std::setw(10) << L[i + j*nrows] << " ";
        }
        std::cout << std::endl;
    }
}

// Note: matrices are stored in column major order; i.e. the array elements in
// the (m x n) matrix C are stored in the sequence: {C_00, C_10, ..., C_m0,
// C_01, C_11, ..., C_m1, C_02, ..., C_0n, C_1n, ..., C_mn}
void MMult0(long m, long n, long k, double *a, double *b, double *c) {
    for (long j = 0; j < n; j++) {
        for (long p = 0; p < k; p++) {
            for (long i = 0; i < m; i++) {
                double A_ip = a[i+p*m];
                double B_pj = b[p+j*k];
                double C_ij = c[i+j*m];
                C_ij = C_ij + A_ip * B_pj;
                c[i+j*m] = C_ij;
            }
        }
    }
}

void MMult0p5(long m, long n, long k, double *a, double *b, double *c) {
    //Rearranged loops
    for (long p = 0; p < k; p++) {
        for (long j = 0; j < n; j++) {
            double B_pj = b[p+j*k];
            for (long i = 0; i < m; i++) {
                double A_ip = a[i+p*m];
                double C_ij = c[i+j*m];
                C_ij = C_ij + A_ip * B_pj;
                c[i+j*m] = C_ij;
            }
        }
    }
}

void MMult1(long m, long n, long k, double *a, double *b, double *c) {
    long M = m/BLOCK_SIZE;  // Number of blocks fitting in a column of A and C
    long K = k/BLOCK_SIZE; // Number of blocks fitting in a row of A or a column of B
    long N = n/BLOCK_SIZE; // Number of blocks fitting in a row of B and C
    long BS2 = BLOCK_SIZE*BLOCK_SIZE; 
    double ablock[BS2]; 
    double bblock[BS2];
    double cblock [BS2];
    int idx; // Generic flat index going from 0 to BS2-1
    int i,j,p; // Local indices for matrix multiplication in a square block
    int row,col; // Local indices for copying back and forth between heap to stack
    long I,J,P; //Indices for which block we're working on 
    double bb; //Value of b block for matrix block multiplication
    //#pragma omp parallel for
    for (I=0; I<M; I++){
        for (J=0; J<N; J++){
            int tid = omp_get_thread_num();
            if (tid == 0){
                int nthr = omp_get_num_threads();
                // printf("There are %d total threads\n", nthr);
            }
            // Initialize the cache C block to be all zeros 
            for (idx=0; idx<BS2; idx++){
                cblock[idx] = 0.0; //c[cidx+idx];
            }
            //printf("Starting thread %d\n", tid);
            //#pragma omp parallel for
            for (P=0; P<K; P++){
                // Loop through the inner dimension
                //Load A and B blocks
                for (col=0; col<BLOCK_SIZE; col++){
                    for (row=0; row<BLOCK_SIZE; row++){
                        ablock[row+BLOCK_SIZE*col] = a[(I*BLOCK_SIZE+row) + (P*BLOCK_SIZE+col)*m];
                        bblock[row+BLOCK_SIZE*col] = b[(P*BLOCK_SIZE+row) + (J*BLOCK_SIZE+col)*k];
                    }
                }
                // Finally, do the multiplication in a fast loop. Speed = cache money
                for (p=0; p<BLOCK_SIZE; p++){
                    for (j=0; j<BLOCK_SIZE; j++){
                        for (i=0; i<BLOCK_SIZE; i++){
                            cblock[i+j*BLOCK_SIZE] += ablock[i+p*BLOCK_SIZE]*bblock[p+j*BLOCK_SIZE];
                        }
                    }
                }
            }
            // Copy the results from cblock into c
            for (col=0; col<BLOCK_SIZE; col++){
                for (row=0; row<BLOCK_SIZE; row++){
                    c[(BLOCK_SIZE*I+row) + (BLOCK_SIZE*J+col)*m] = cblock[row+BLOCK_SIZE*col];
                }
            }
        }
    }
}

void MMult2(long m, long n, long k, double *a, double *b, double *c) {
    long M = m/BLOCK_SIZE;  // Number of blocks fitting in a column of A and C
    long K = k/BLOCK_SIZE; // Number of blocks fitting in a row of A or a column of B
    long N = n/BLOCK_SIZE; // Number of blocks fitting in a row of B and C
    long BS2 = BLOCK_SIZE*BLOCK_SIZE; 
    double ablock[BS2]; 
    double bblock[BS2];
    double cblock [BS2];
    int idx; // Generic flat index going from 0 to BS2-1
    int i,j,p; // Local indices for matrix multiplication in a square block
    int row,col; // Local indices for copying back and forth between heap to stack
    long I,J,P; //Indices for which block we're working on 
    double bb; //Value of b block for matrix block multiplication
    #pragma omp parallel for num_threads(6)
    for (I=0; I<M; I++){
        for (J=0; J<N; J++){
            int tid = omp_get_thread_num();
            if (tid == 0){
                int nthr = omp_get_num_threads();
                // printf("There are %d total threads\n", nthr);
            }
            // Initialize the cache C block to be all zeros 
            for (idx=0; idx<BS2; idx++){
                cblock[idx] = 0.0; //c[cidx+idx];
            }
            //printf("Starting thread %d\n", tid);
            for (P=0; P<K; P++){
                // Loop through the inner dimension
                // #pragma omp parallel for
                //Load A and B blocks
                for (col=0; col<BLOCK_SIZE; col++){
                    for (row=0; row<BLOCK_SIZE; row++){
                        ablock[row+BLOCK_SIZE*col] = a[(I*BLOCK_SIZE+row) + (P*BLOCK_SIZE+col)*m];
                        bblock[row+BLOCK_SIZE*col] = b[(P*BLOCK_SIZE+row) + (J*BLOCK_SIZE+col)*k];
                    }
                }
                // Finally, do the multiplication in a fast loop. Speed = cache money
                for (p=0; p<BLOCK_SIZE; p++){
                    for (j=0; j<BLOCK_SIZE; j++){
                        bb = bblock[p+j*BLOCK_SIZE];
                        for (i=0; i<BLOCK_SIZE; i++){
                            cblock[i+j*BLOCK_SIZE] += ablock[i+p*BLOCK_SIZE]*bblock[p+j*BLOCK_SIZE];
                        }
                    }
                }
            }
            // Copy the results from cblock into c
            for (col=0; col<BLOCK_SIZE; col++){
                for (row=0; row<BLOCK_SIZE; row++){
                    c[(BLOCK_SIZE*I+row) + (BLOCK_SIZE*J+col)*m] = cblock[row+BLOCK_SIZE*col];
                }
            }
        }
    }
}

void loop_order_comparison(){
    // Time matrix multiplication for the default order and rearranged loops
    const long PFIRST = BLOCK_SIZE;
    const long PLAST = 1000; //2000;
    const long PINC = std::max(50/BLOCK_SIZE,1) * BLOCK_SIZE; // multiple of BLOCK_SIZE
    printf(" Dimension    Time(0)     Time(1)    Gflop/s(0)  Gflop/s(1) GB/s(0)   GB/s (1)  Error\n");
    for (long p = PFIRST; p < PLAST; p += PINC) {
        long m = p, n = p, k = p;
        long NREPEATS = 2; //1e9/(m*n*k)+1;
        double* a = (double*) aligned_malloc(m * k * sizeof(double)); // m x k
        double* b = (double*) aligned_malloc(k * n * sizeof(double)); // k x n
        double* c = (double*) aligned_malloc(m * n * sizeof(double)); // m x n
        double* c_ref = (double*) aligned_malloc(m * n * sizeof(double)); // m x n
  
        // Initialize matrices
        for (long i = 0; i < m*k; i++) a[i] = drand48();
        for (long i = 0; i < k*n; i++) b[i] = drand48();
        
        // Basic matrix multiplication
        Timer t0;
        t0.tic();
        for (long rep = 0; rep < NREPEATS; rep++) { // Compute reference solution
            for (long i = 0; i < m*n; i++) c_ref[i] = 0;
            MMult0(m, n, k, a, b, c_ref);
        }
        double time0 = t0.toc();
        double bandwidth0 = (4*m*n*k + m*n)*NREPEATS*sizeof(double) / (time0 * 1e9); 
        double Gflops0 = m*n*k*NREPEATS / (time0 * 1e9);

        Timer t;
        t.tic();
        for (long rep = 0; rep < NREPEATS; rep++) {
            for (long i = 0; i < m*n; i++) c[i] = 0;
            MMult0p5(m, n, k, a, b, c);
        }
        double time = t.toc();
        double bandwidth = (4*m*n*k + m*n)*NREPEATS*sizeof(double) / (time * 1e9); 
        double Gflops =  m*n*k*NREPEATS / (time * 1e9);  //TODO: calculate from m, n, k, NREPEATS, time
        printf("%10d %10f %10f %10f %10f %10f %10f", p, time0, time, Gflops0, Gflops, bandwidth0, bandwidth);
        double max_err = 0;
        for (long i = 0; i < m*n; i++) max_err = std::max(max_err, fabs(c[i] - c_ref[i]));
        printf(" %10e\n", max_err);
  
        aligned_free(a);
        aligned_free(b);
        aligned_free(c);
        aligned_free(c_ref);
    }
}
void block_omp_comparison(){
    // Time matrix multiplication for the default order and rearranged loops
    const long PFIRST = BLOCK_SIZE;
    const long PLAST = 1000; //2000;
    const long PINC = std::max(50/BLOCK_SIZE,1) * BLOCK_SIZE; // multiple of BLOCK_SIZE
    printf(" Dimension Time (0)  Time (1)  Gflop/s(0)  Gflop/s(1) GB/s(0)   GB/s (1)  Error\n");
    for (long p = PFIRST; p < PLAST; p += PINC) {
        long m = p, n = p, k = p;
        long NREPEATS = 2; //1e9/(m*n*k)+1;
        double* a = (double*) aligned_malloc(m * k * sizeof(double)); // m x k
        double* b = (double*) aligned_malloc(k * n * sizeof(double)); // k x n
        double* c = (double*) aligned_malloc(m * n * sizeof(double)); // m x n
        double* c_ref = (double*) aligned_malloc(m * n * sizeof(double)); // m x n
  
        // Initialize matrices
        for (long i = 0; i < m*k; i++) a[i] = drand48();
        for (long i = 0; i < k*n; i++) b[i] = drand48();
        
        // Basic matrix multiplication
        Timer t0;
        t0.tic();
        for (long rep = 0; rep < NREPEATS; rep++) { // Compute reference solution
            for (long i = 0; i < m*n; i++) c_ref[i] = 0;
            MMult0(m, n, k, a, b, c_ref);
        }
        double time0 = t0.toc();
        double bandwidth0 = (4*m*n*k + m*n)*NREPEATS*sizeof(double) / (time0 * 1e9); 
        double Gflops0 = m*n*k*NREPEATS / (time0 * 1e9);

        Timer t;
        t.tic();
        for (long rep = 0; rep < NREPEATS; rep++) {
            for (long i = 0; i < m*n; i++) c[i] = 0;
            MMult2(m, n, k, a, b, c);
        }
        double time = t.toc();
        long M = m/BLOCK_SIZE;  // Number of blocks fitting in a column of A and C
        long K = k/BLOCK_SIZE; // Number of blocks fitting in a row of A or a column of B
        long N = n/BLOCK_SIZE; // Number of blocks fitting in a row of B and C
        double bandwidth = N*M*(K*pow(BLOCK_SIZE,2) + pow(BLOCK_SIZE,3) + pow(BLOCK_SIZE,2));
        bandwidth += m*n; //for re-initializing c
        bandwidth *= sizeof(double) * NREPEATS / (time * 1e9);
        double Gflops =  m*n*k*NREPEATS / (time * 1e9);  //TODO: calculate from m, n, k, NREPEATS, time
        printf("%10d %10f %10f %10f %10f %10f %10f", p, time0, time, Gflops0, Gflops, bandwidth0, bandwidth);
        double max_err = 0;
        for (long i = 0; i < m*n; i++) max_err = std::max(max_err, fabs(c[i] - c_ref[i]));
        printf(" %10e\n", max_err);
  
        aligned_free(a);
        aligned_free(b);
        aligned_free(c);
        aligned_free(c_ref);
    }

}

void block_comparison(){
    // Time matrix multiplication for the default order and rearranged loops
    const long PFIRST = BLOCK_SIZE;
    const long PLAST = 1000; //2000;
    const long PINC = std::max(50/BLOCK_SIZE,1) * BLOCK_SIZE; // multiple of BLOCK_SIZE
    printf(" Dimension Time (0)  Time (1)  Gflop/s(0)  Gflop/s(1) GB/s(0)   GB/s (1)  Error\n");
    for (long p = PFIRST; p < PLAST; p += PINC) {
        long m = p, n = p, k = p;
        long NREPEATS = 2; //1e9/(m*n*k)+1;
        double* a = (double*) aligned_malloc(m * k * sizeof(double)); // m x k
        double* b = (double*) aligned_malloc(k * n * sizeof(double)); // k x n
        double* c = (double*) aligned_malloc(m * n * sizeof(double)); // m x n
        double* c_ref = (double*) aligned_malloc(m * n * sizeof(double)); // m x n
  
        // Initialize matrices
        for (long i = 0; i < m*k; i++) a[i] = drand48();
        for (long i = 0; i < k*n; i++) b[i] = drand48();
        
        // Basic matrix multiplication
        Timer t0;
        t0.tic();
        for (long rep = 0; rep < NREPEATS; rep++) { // Compute reference solution
            for (long i = 0; i < m*n; i++) c_ref[i] = 0;
            MMult0(m, n, k, a, b, c_ref);
        }
        double time0 = t0.toc();
        double bandwidth0 = (4*m*n*k + m*n)*NREPEATS*sizeof(double) / (time0 * 1e9); 
        double Gflops0 = m*n*k*NREPEATS / (time0 * 1e9);

        Timer t;
        t.tic();
        for (long rep = 0; rep < NREPEATS; rep++) {
            for (long i = 0; i < m*n; i++) c[i] = 0;
            MMult1(m, n, k, a, b, c);
        }
        double time = t.toc();
        long M = m/BLOCK_SIZE;  // Number of blocks fitting in a column of A and C
        long K = k/BLOCK_SIZE; // Number of blocks fitting in a row of A or a column of B
        long N = n/BLOCK_SIZE; // Number of blocks fitting in a row of B and C
        double bandwidth = N*M*(K*pow(BLOCK_SIZE,2) + pow(BLOCK_SIZE,3) + pow(BLOCK_SIZE,2));
        bandwidth += m*n; //for re-initializing c
        bandwidth *= sizeof(double) * NREPEATS / (time * 1e9);
        double Gflops =  m*n*k*NREPEATS / (time * 1e9);  //TODO: calculate from m, n, k, NREPEATS, time
        printf("%10d %10f %10f %10f %10f %10f %10f", p, time0, time, Gflops0, Gflops, bandwidth0, bandwidth);
        double max_err = 0;
        for (long i = 0; i < m*n; i++) max_err = std::max(max_err, fabs(c[i] - c_ref[i]));
        printf(" %10e\n", max_err);
  
        aligned_free(a);
        aligned_free(b);
        aligned_free(c);
        aligned_free(c_ref);
    }

}

int main(int argc, char** argv) {
    long doubles_in_cache = 35840.0e3 / sizeof(double);
    printf("Number of doubles that fit in cache = %10ld = %10f^2\n", doubles_in_cache, sqrt((double)doubles_in_cache)); 
    int option = atoi(argv[1]);
    if (option == 0){
        loop_order_comparison();
    } else if (option == 1){
        block_comparison();
    } else {
        block_omp_comparison();
    }
    return 0;
}

// * Using MMult0 as a reference, implement MMult1 and try to rearrange loops to
// maximize performance. Measure performance for different loop arrangements and
// try to reason why you get the best performance for a particular order?
//
//
// * You will notice that the performance degrades for larger matrix sizes that
// do not fit in the cache. To improve the performance for larger matrices,
// implement a one level blocking scheme by using BLOCK_SIZE macro as the block
// size. By partitioning big matrices into smaller blocks that fit in the cache
// and multiplying these blocks together at a time, we can reduce the number of
// accesses to main memory. This resolves the main memory bandwidth bottleneck
// for large matrices and improves performance.
//
// NOTE: You can assume that the matrix dimensions are multiples of BLOCK_SIZE.
//
//
// * Experiment with different values for BLOCK_SIZE (use multiples of 4) and
// measure performance.  What is the optimal value for BLOCK_SIZE?
//
//
// * Now parallelize your matrix-matrix multiplication code using OpenMP.
//
//
// * What percentage of the peak FLOP-rate do you achieve with your code?
//
//
// NOTE: Compile your code using the flag -march=native. This tells the compiler
// to generate the best output using the instruction set supported by your CPU
// architecture. Also, try using either of -O2 or -O3 optimization level flags.
// Be aware that -O2 can sometimes generate better output than using -O3 for
// programmer optimized code.
