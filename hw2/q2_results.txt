The file MMult1.cpp contains four different implementations of matrix-matrix multiplication. 
First is MMult0, the naive nested for-loop implementation. Second is MMult0p5, which I experimented with rearranging loops. The matrices are stored in column-major order, and this default method is the baseline for measuring performance.

Second is MMult0p5, in which I experimented with the loop order. I found that as long as the outer index i was the innermost variable, all orders had a similar performance. The element access pattern makes it easy to see why: i has spatial locality in both the a and c matrices, which constitute two of the three memory access bottlenecks. In other words, when i increases by one, the corresponding entries of a and c are usually the next slot in the cache line. Had I chosen j or p as the innermost variable, only one of the three matrices would have enjoyed this spatial locality, not two of three. Timings for this versus the default order is listed in the files order_<B>.txt, for various block sizes B.

Third is MMult1, which partitions the matrices into square blocks determined by the BLOCK_SIZE macro. The idea is that this reduces the number of slow memory read operations by a factor of ~1/BLOCK_SIZE. Timings for this version are in block_<B>.txt

Fourth is MMult2, which parallelizes MMult1 with an openmp parallel for loop around the outermost dimension. The innermost loop of course cannot be parallelized, because an entire block of c is being incremented. Timings are reported in block_omp_<B>.txt. 

I could not eke out a speedup with only blocking. Adding OpenMP did achieve a very marginal speedup for some large matrix sizes. 



