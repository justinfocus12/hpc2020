#include <iostream>
#include <cmath>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iomanip>

#define BS 2

/*
Everything is in column-major order here!
 */

void print_matrix(double *L, int nrows, int ncols){
    //Print out the flattened (nrows)x(ncols) matrix L in a formatted way
    for (int i=0; i<nrows; i++){
        for (int j=0; j<ncols; j++){
            std::cout << std::setprecision(4) << std::setw(10) << L[i + j*nrows] << " ";
        }
        std::cout << std::endl;
    }
}

void print_array(double *x, int n){
    //Print out an n-component vector
    printf("      ");
    for (int i=0; i<n; i++){
        //printf("%0.3f    ", x[i]);
        std::cout << std::setprecision(4) << std::setw(10) << x[i] << " ";
    }
    printf("\n");
}

void matmul(double*a, double*b, double*c, int m, int n, int p){
    int i,j,k;
    for (i=0; i<m; i++){
        for (j=0; j<p; j++){
            for (k=0; k<n; k++){
                c[i+m*j] += a[i+n*k]*b[k+p*j];
            }
        }
    }
}

void block_matmul(double* a, double* b, double* c, int m, int n, int p){
    //BS = blocksize. Assume explicitly that BS is a divisor of m, n and p
    int M = m/BS;
    int N = n/BS;
    int P = p/BS;
    int BS2 = BS*BS;
    int idx; // Generic flat index, going from 0 to BS^2-1
    int I,J,K; //Points to the block we're on
    int i,j,k; //Local indices for multiplying square blocks
    int row,col; //Local indices for copying chunks of A and B into fast memory
    double ablock[BS2];
    double bblock[BS2];
    double cblock[BS2];
    for (I=0; I<M; I++){
        for (J=0; J<P; J++){
            // Set cblock to be zeros
            for (idx=0; idx<BS2; idx++) cblock[idx] = 0.0;
            // Loop through inner dimension
            for (K=0; K<N; K++){
                // Load ablock and bblock
                for (row=0; row<BS; row++){
                    for (col=0; col<BS; col++){
                        ablock[row + col*BS] = a[(I*BS+row) + (K*BS+col)*m];
                        bblock[row + col*BS] = b[(K*BS+row) + (J*BS+col)*n];
                    }
                }
                // Compute the c block
                for (i=0; i<BS; i++){
                    for (j=0; j<BS; j++){
                        for (k=0; k<BS; k++){
                            cblock[i+j*BS] += ablock[i+k*BS]*bblock[k+j*BS];
                        }
                    }
                }
            }
            // Copy the cblock into memory  
            for (col=0; col<BS; col++){
                for (row=0; row<BS; row++){
                    c[(I*BS+row) + (J*BS+col)*m] = cblock[row + col*BS];
                }
            }
        }
    }
}

int main(int argc, char** argv){
    int m=4,n=4,p=4;
    int i;
    double* a = (double*) malloc(m*n*sizeof(double));
    for (i=0; i<m*n; i++) a[i] = double(rand())/RAND_MAX; //i
    double* b = (double*) malloc(n*p*sizeof(double));
    for (i=0; i<n*p; i++) b[i] = double(rand())/RAND_MAX; //i+m*n;
    double* c = (double*) malloc(m*p*sizeof(double));
    double* c_blocked = (double*) malloc(m*p*sizeof(double));
    for (int rep=0; rep<2; rep++){    
        for (i=0; i<m*n; i++){
            c[i] = 0.0;
            c_blocked[i] = 0.0;
        }
        printf("A = \n");
        print_matrix(a,m,n);
        printf("B = \n");
        print_matrix(b,n,p);
        
        //Reference solution
        printf("Regular matrix multiplication\n");
        matmul(a,b,c,m,n,p);
        print_matrix(c,m,p);

        // Blocked solution
        block_matmul(a,b,c_blocked,m,n,p);
        printf("Blocked matrix multiplication\n");
        print_matrix(c_blocked,m,p);

        // Compute the error
        double maxerr = 0.0;
        for (i=0; i<m*p; i++) maxerr = std::max(maxerr, std::abs(c[i]-c_blocked[i]));
        printf("Maximum error = %1.10f\n", maxerr);
    }
    free(a);
    free(b);
    free(c);
}

