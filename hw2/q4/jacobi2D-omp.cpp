#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <omp.h>
#include "utils.h"

//Column-major order is our convention here
double compute_residual(double* u, double* f, int n){
    double ressq=0.0,locres=0; //squared residual and local residual
    double h2 = 1.0/pow(n+1,2);
    int n2 = pow(n,2);
    int i,j,k;
    for (k=0; k<n2; k++){
        j = k / n;
        i = k - j*n;
        // Compute the residual of the old solution
        locres = -4*u[k];
        if (i<n-1) locres += u[k+1];
        if (i>0) locres += u[k-1];
        if (j<n-1) locres += u[k+n];
        if (j>0) locres += u[k-n];
        locres *= 1.0/h2;
        locres += f[k];
        ressq += pow(locres,2);
    }
    return sqrt(ressq);
}

void jacobi_iteration(double* u, double* f, int n, int maxiter, int nthreads, double* residual, int num_residuals, bool print_iters){
    //Perform the Jacobi update, using not matrices but the explicit expression
    //Don't update in place, but rather put the new result in unew. Copy unew to u at each iteration
    double h2 = 1.0/((n+1)*(n+1)); //pow(n+1,2);
    int n2 = n*n;
    double* unew = (double*) malloc(n2 * sizeof(double));
    int iter;
    int res_idx = 0; // which residual we're on 
    //printf("Iteration      Residual\n");
    //printf("%9d %16f\n", 0, compute_residual(u,f,n));
    for (iter=0; iter<maxiter; iter++){
        #pragma omp parallel for num_threads(nthreads)
        for (int k=0; k<n2; k++){
            int tid = omp_get_thread_num();
            //printf("Starting thread %d\n", tid);
            //if (iter == 0 && k == 0){ 
            //    printf("From thread %d: there are %d threads\n", tid, omp_get_num_threads());
            //}
            int j = k / n;
            int i = k - j*n;
            unew[k] = h2*f[k];
            if (i<n-1) unew[k] += u[k+1];
            if (i>0) unew[k] += u[k-1];
            if (j<n-1) unew[k] += u[k+n];
            if (j>0) unew[k] += u[k-n];
            unew[k] *= 0.25;
        }
        for (int k=0; k<n2; k++){
            u[k] = unew[k];
        }
        if (print_iters && iter % (maxiter/num_residuals)  == (maxiter/num_residuals-1)){
            residual[res_idx] = compute_residual(u,f,n);
            res_idx++;
            //printf("%4d  %12f\n", iter, compute_residual(u,f,n));
        }
    }
    residual[num_residuals-1] = compute_residual(u,f,n);
    free(unew);
}

void time_jacobi(int N, int nthreads, int maxiter, double* time, double* residual, int num_residuals, bool print_iters){
    // Run Jacobi iteration 
    int n = N-1; //Number of interior grid points along an edge
    int n2 = n*n; 
    double* u = (double*) malloc(n2 * sizeof(double)); //Solution vector (flattened)
    double* f = (double*) malloc(n2 * sizeof(double)); //Right hand side
    int k;
    for (k=0; k<n2; k++){
        u[k] = 0.0;
        f[k] = 1.0;
    }
    Timer t;
    t.tic();
    jacobi_iteration(u, f, n, maxiter, nthreads, residual, num_residuals, print_iters);
    *time = t.toc();
    //printf("Time = %10f\n",duration);
    free(u);
    free(f);
}

void display_jacobi(){
    int maxiter = 1000;
    int N = 100;
    int nthreads = 10;
    double time;
    int num_residuals = 10;
    double residual[num_residuals]; 
    time_jacobi(N, nthreads, maxiter, &time, residual, num_residuals, 1);
}

int main(int argc, char** argv){
    //display_jacobi();
    int maxiter = 5000;
    int Nmax = 1000;
    int nthreads_max = 10;
    int N, nthreads;
    double time; // These will be computed by the algorithm
    int num_residuals = 10;
    double residual[num_residuals];
    // Run through several cases and print out the results in a table
    printf("N     nthreads       time    residuals (every %d iterations)\n", maxiter/num_residuals);
    for (N = Nmax/10; N <= Nmax; N += Nmax/10){
        for (nthreads = 1; nthreads <= nthreads_max; nthreads += 4){
            time_jacobi(N, nthreads, maxiter, &time, residual, num_residuals, 1);
            printf("%3d %9d %10f ", N, nthreads, time);
            for (int r=0; r<num_residuals; r++){
                printf("  %10f  ", residual[r]);
            }
            printf("\n");
        }
    }
    return 0;   
}
    


