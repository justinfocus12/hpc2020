/******************************************************************************
* FILE: omp_bug5.c
* DESCRIPTION:
*   Using SECTIONS, two threads initialize their own array and then add
*   it to the other's array, however a deadlock occurs.
* AUTHOR: Blaise Barney  01/29/04
* LAST REVISED: 04/06/05
******************************************************************************/

/*
 * BUG: The main issue here was deadlock. However, even commenting out the entire meat of the program, there were still memory issues
 * with threads accessing each other's stacks.
 * FIX: First, I changed a and b to be dynamically allocated heap memory with malloc. Then, I changed the order of setting and unsetting
 * the locks. The problem is that both sections are being run at the same time, and while one changes a, the other uses b (and vice versa). 
 * To ensure that a is never being changed while being used, I locked both a and b at the very start of each section, and 
 * unlocked them at the very end. This doesn't solve the underlying race condition of these two sections, which should be addressed by 
 * some kind of barrier (or just ditching parallelism altogether), but this depends on what the actual goal of the code should be.
 */

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#define N 1000000
#define PI 3.1415926535
#define DELTA .01415926535

int main (int argc, char *argv[]) 
{
int nthreads, tid, i;
//float a[N], b[N];
float* a = (float*) malloc(N*sizeof(float));
float* b = (float*) malloc(N*sizeof(float));
for (int n=0; n<N; n++){ a[n] = 0; b[n] = 0; }
omp_lock_t locka, lockb;

/* Initialize the locks */
omp_init_lock(&locka);
omp_init_lock(&lockb);

/* Fork a team of threads giving them their own copies of variables */
#pragma omp parallel shared(a, b, nthreads, locka, lockb) private(tid) num_threads(4)
  {

  /* Obtain thread number and number of threads */
  tid = omp_get_thread_num();
  #pragma omp master
    {
    nthreads = omp_get_num_threads();
    printf("Number of threads = %d\n", nthreads);
    }
  printf("Thread %d starting...\n", tid);
  #pragma omp barrier

  #pragma omp sections nowait
    {
    #pragma omp section
      {
      printf("Thread %d initializing a[]\n",tid);
      //omp_set_lock(&locka);
      //omp_set_lock(&lockb);
      for (i=0; i<N; i++)
        a[i] = i * DELTA;
      //omp_set_lock(&lockb);
      
      printf("Thread %d adding a[] to b[]\n",tid);
      for (i=0; i<N; i++)
        b[i] += a[i];
      //omp_unset_lock(&lockb);
      //omp_unset_lock(&locka);
      }

    #pragma omp section
      {
      printf("Thread %d initializing b[]\n",tid);
      //omp_set_lock(&lockb);
      //omp_set_lock(&locka);
      for (i=0; i<N; i++)
        b[i] = i * PI;
      //omp_set_lock(&locka);
      printf("Thread %d adding b[] to a[]\n",tid);
      for (i=0; i<N; i++)
        a[i] += b[i];
      //omp_unset_lock(&locka);
      //omp_unset_lock(&lockb);
      }

    }  /* end of sections */
  }  /* end of parallel region */

free(a);
free(b);

}

