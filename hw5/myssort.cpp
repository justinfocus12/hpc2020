// Parallel sample sort
#include <stdio.h>
#include <unistd.h>
#include <mpi.h>
#include <stdlib.h>
#include <algorithm>

int main( int argc, char *argv[]) {
    MPI_Init(&argc, &argv);
    
    int rank, p;
    double start, end;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &p);
    
    // Number of random numbers per processor (this should be increased
    // for actual tests or could be passed in through the command line
    int N = atoi(argv[1]);
    
    int* vec = (int*) malloc(N*sizeof(int));
    // seed random number generator differently on every core
    srand((unsigned int) (rank + 393919));
    
    // fill vector with random integers
    for (int i = 0; i < N; ++i) {
        vec[i] = rand(); //rank + i*p; //rand();
    }
    // printf("rank: %d, first entry: %d\n", rank, vec[0]);

    if (rank == 0) start = MPI_Wtime();
    // sort locally
    std::sort(vec, vec+N);
    
    // sample p-1 entries from vector as the local splitters, i.e.,
    // every N/P-th entry of the sorted vector
    // the i'th splitter will be the inclusive upper bound of the i'th bucket
    int loc_split[p-1];
    for (int i=0; i<p-1; i++){
        loc_split[i] = vec[(N/p)*(i+1)];
    }
    // printf("Process %d splitters: ", rank);
    // for (int i=0; i<p-1; i++) printf("%d, ", loc_split[i]);
    // printf("\n");
    
    // every process communicates the selected entries to the root
    // process; use for instance an MPI_Gather
    int loc_split_gather[p*(p-1)];
    MPI_Gather(loc_split, p-1, MPI_INT, loc_split_gather+rank*(p-1), p-1, MPI_INT, 0, MPI_COMM_WORLD);

    // root process does a sort and picks (p-1) splitters (from the
    // p(p-1) received elements)
    int glob_split[p-1];
    if (rank == 0){
        // printf("Gathered splitters: ");
        // for (int i=0; i<p*(p-1); i++) printf("%d, ", loc_split_gather[i]);
        // printf("\n");
        std::sort(loc_split_gather, loc_split_gather + p*(p-1));
        for (int i=0; i<p-1; i++){
            glob_split[i] = loc_split_gather[(p-1)*(i+1)];
        }
        // printf("Global splitters: ");
        // for (int i=0; i<p-1; i++) printf("%d, ", glob_split[i]);
        // printf("\n");
    }
    MPI_Barrier(MPI_COMM_WORLD);
    // root process broadcasts splitters to all other processes
    MPI_Bcast(glob_split, (p-1), MPI_INT, 0, MPI_COMM_WORLD);

    // every process uses the obtained splitters to decide which
    // integers need to be sent to which other process (local bins).
    // Note that the vector is already locally sorted and so are the
    // splitters; therefore, we can use std::lower_bound function to
    // determine the bins efficiently.
    //
    // Hint: the MPI_Alltoallv exchange in the next step requires
    // send-counts and send-displacements to each process. Determining the
    // bins for an already sorted array just means to determine these
    // counts and displacements. For a splitter s[i], the corresponding
    // send-displacement for the message to process (i+1) is then given by,
    // sdispls[i+1] = std::lower_bound(vec, vec+N, s[i]) - vec;
    
    // send and receive: first use an MPI_Alltoall to share with every
    // process how many integers it should expect, and then use
    // MPI_Alltoallv to exchange the data

    int sendcounts[p];
    int sdispls[p];
    sdispls[0] = 0;
    for (int i=0; i<p-1; i++){
        sdispls[i+1] = std::lower_bound(vec, vec+N, glob_split[i]) - vec;
        sendcounts[i] = sdispls[i+1] - sdispls[i];
    }
    sendcounts[p-1] = N - sdispls[p-1];
   
     
    // printf("Rank %d is sending these many: ", rank);
    // for (int i=0; i<p; i++) printf("%d, ", sendcounts[i]);
    // printf("\n");
    // printf("Rank %d send displacements: ", rank);
    // for (int i=0; i<p; i++) printf("%d, ", sdispls[i]);
    // printf("\n");
    // MPI_Barrier(MPI_COMM_WORLD);
    
    int recvcounts[p];
    int rdispls[p];
    //MPI_Alltoall(sdispls, 1, MPI_INT, rdispls, 1, MPI_INT, MPI_COMM_WORLD);
    MPI_Alltoall(sendcounts, 1, MPI_INT, recvcounts, 1, MPI_INT, MPI_COMM_WORLD);
   
    // printf("Rank %d receive counts: ", rank);
    // for (int i=0; i<p; i++) printf("%d, ", recvcounts[i]);
    // printf("\n");
    // MPI_Barrier(MPI_COMM_WORLD);

    int mysize = 0;
    rdispls[0] = 0;
    for (int i=0; i<p-1; i++){
        mysize += recvcounts[i];
        rdispls[i+1] = rdispls[i] + recvcounts[i];
    }
    mysize += recvcounts[p-1];
    // printf("Rank %d newsize=%d, receive displacements: ", rank, mysize);
    // for (int i=0; i<p; i++) printf("%d, ", rdispls[i]);
    // printf("\n");
    // MPI_Barrier(MPI_COMM_WORLD);
    
    int* newvec = (int*) malloc(mysize*sizeof(int));
    // printf("Process %d before alltoallv: vec = ", rank);
    // for (int i=0; i<N; i++) printf("%d, ", vec[i]); 
    // printf("\n");
    // MPI_Barrier(MPI_COMM_WORLD);

    MPI_Alltoallv(vec, sendcounts, sdispls, MPI_INT, 
                  newvec, recvcounts, rdispls, MPI_INT, MPI_COMM_WORLD);
    // printf("Process %d after alltoallv: newvec = ", rank);
    // for (int i=0; i<mysize; i++) printf("%d, ", newvec[i]); 
    // printf("\n");
    // MPI_Barrier(MPI_COMM_WORLD);
    
    // do a local sort of the received data
    std::sort(newvec, newvec + mysize);
    
    if (rank == 0) end = MPI_Wtime();

    // every process writes its result to a file
    FILE* fd = NULL;
    char filename[256];
    snprintf(filename, 256, "ssort_np%d_N%d_proc%d_sorted.txt", p, N, rank);
    fd = fopen(filename, "w");
    if (NULL == fd){
        printf("Error opening file \n");
        return 1;
    }
    fprintf(fd, "Processor %d array", rank);
    //printf("Processor %d array: ", rank);
    if (rank == 0) fprintf(fd, " Time %f", end-start);
    fprintf(fd, "\n");
    for (int i=0; i<mysize; i++){
        fprintf(fd, "%d\n", newvec[i]);
        //printf("%d, ", newvec[i]);
    }
    fclose(fd);
    //printf("\n");

    
    free(newvec);
    free(vec);
    //MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}
