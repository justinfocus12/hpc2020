/*
 * Implement 2D Jacobi smoother to solve -laplacian u = 1 on (0,1) x (0,1)
 */

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <mpi.h>

int main(int argc, char** argv){
    // Read in three command line arguments: Nl (side length per processor) and maxiter
    // where there are Nl^2*p total unknowns, p being the number of processors 4^j
    int Nl = atoi(argv[1]);
    int maxiter = atoi(argv[2]);
    int rank, numproc, log4numproc, log2numproc, four_to_the_log4numproc, I, J, K, i, j, k;
    double h, h2, loc_res, glob_res, left, right, up, down;
    MPI_Init(NULL, NULL); //(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &numproc);

    // Check that the number of processors is indeed a power of 4
    log4numproc = 0;
    log2numproc = 1;
    four_to_the_log4numproc = 1;
    while (four_to_the_log4numproc < numproc){
        log4numproc++;
        four_to_the_log4numproc *= 4;
        log2numproc *= 2;
        //printf("while loop: log4numproc = %d\n", log4numproc);
    }
    if (four_to_the_log4numproc != numproc){
        printf("Error! Number of processors must be a power of 4.\n");
        return 1;
    }
    // Some numbers
    h = 1.0/(Nl*log2numproc + 1);
    h2 = pow(h,2);
    //printf("h = %f, log4numproc = %d, log2numproc = %d\n", h, log4numproc, log2numproc);
    // Initialize local patch to all zeros
    double* u_loc = (double*) malloc(Nl*Nl*sizeof(double));
    double* unew_loc = (double*) malloc(Nl*Nl*sizeof(double)); // Update
    double* utemp;
    for (i=0; i<Nl*Nl; i++){
        u_loc[i] = 0.0;
        unew_loc[i] = 0.0;
    }
    // Separate memory for ghost cells. Order: (I-1,J), (I+1,J), (I,J-1), (I,J+1)
    double* u_ghost_recv = (double*) malloc(4*Nl*sizeof(double));
    double* u_ghost_send = (double*) malloc(4*Nl*sizeof(double));
    for (i=0; i<4*Nl; i++) u_ghost_recv[i] = 0.0; u_ghost_send[i] = 0.0;
    // Where on the grid are we? rank = I*log2numproc + J (row-major order)
    I = rank / log2numproc;
    J = rank - I*log2numproc;
    FILE* fd = NULL;
    double start, end;
    if (rank == 0){
        char filename[256];
        snprintf(filename, 256, "jacobi_np%d_Nl%d.txt", numproc, Nl);
        fd = fopen(filename, "w");
        if (NULL == fd){
            printf("Error opening file \n");
            return 1;
        }
        fprintf(fd, "Iteration    Residual     log10(residual)\n", rank);
        start = MPI_Wtime();
    }
    // Send and receive messages
    MPI_Status status0, status1, status2, status3;
    for (int iter=0; iter<maxiter; iter++){
        if (I >= 1){
            MPI_Send(&u_ghost_send[0], Nl, MPI_DOUBLE, rank-log2numproc, 0, MPI_COMM_WORLD);
            MPI_Recv(&u_ghost_recv[0], Nl, MPI_DOUBLE, rank-log2numproc, 1, MPI_COMM_WORLD, &status0);
        } 
        if (I <= log2numproc-2){
            MPI_Send(&u_ghost_send[Nl], Nl, MPI_DOUBLE, rank+log2numproc, 1, MPI_COMM_WORLD);
            MPI_Recv(&u_ghost_recv[Nl], Nl, MPI_DOUBLE, rank+log2numproc, 0, MPI_COMM_WORLD, &status1);
        }
        if (J >= 1){
            MPI_Send(&u_ghost_send[2*Nl], Nl, MPI_DOUBLE, rank-1, 2, MPI_COMM_WORLD);
            MPI_Recv(&u_ghost_recv[2*Nl], Nl, MPI_DOUBLE, rank-1, 3, MPI_COMM_WORLD, &status2);
        }
        if (J <= log2numproc-2){ 
            MPI_Send(&u_ghost_send[3*Nl], Nl, MPI_DOUBLE, rank+1, 3, MPI_COMM_WORLD);
            MPI_Recv(&u_ghost_recv[3*Nl], Nl, MPI_DOUBLE, rank+1, 2, MPI_COMM_WORLD, &status3);
        }
        // Compute the residual
        loc_res = 0.0;
        k = 0;
        for (i=0; i<Nl; i++){
            for (j=0; j<Nl; j++){
                left = 0.0; right = 0.0; up = 0.0; down = 0.0;
                if (i >= 1){
                    left = u_loc[k-Nl];
                } else {
                    left = u_ghost_recv[i];
                }
                if (i <= Nl-2){
                    right = u_loc[k+Nl];
                } else {
                    right = u_ghost_recv[Nl+i];
                }
                if (j >= 1){
                    down = u_loc[k-1];
                } else {
                    down = u_ghost_recv[2*Nl+j];
                }
                if (j <= Nl-2){
                    up = u_loc[k+1];
                } else {
                    up = u_ghost_recv[3*Nl+j];
                }
                unew_loc[k] = h2 + left + right + down + up;
                unew_loc[k] *= 0.25;
                loc_res += pow(unew_loc[k] - u_loc[k], 2)/h2;
                k++;
            }
        }
        loc_res = sqrt(loc_res);
        // Switch pointers
        utemp = u_loc;
        u_loc = unew_loc;
        unew_loc = utemp;
        //MPI_Barrier(MPI_COMM_WORLD);
        glob_res = 0.0;
        if (iter % 100 == 0){
            MPI_Allreduce(&loc_res, &glob_res, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
            if (rank == 0){
                fprintf(fd, "%10d    %10f     %10f\n", iter, glob_res, log10(glob_res));
            }
        }
        //printf("Iteration %d, processor %d did the Jacobi update\n", iter, rank);
    }
    if (rank == 0){
        end = MPI_Wtime();
        fprintf(fd,"Time %10f\n", end-start);
        fprintf(fd, "Nl %3d\n", Nl);
        fprintf(fd, "numproc %3d\n", numproc);
        fclose(fd);
    }
    free(u_ghost_recv);
    free(u_ghost_send);
    free(unew_loc);
    free(u_loc);

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();

    return 0;
}

    

