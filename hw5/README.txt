This folder contains two relevant source code files: jacobi2D-mpi.cpp, and myssort.cpp, which can be compiled
together with Makefile. The files file-io.cpp, ssort.cpp, and Makefile0 were provided with the homework assignment
but not needed anymore.

-----------------------
    2D Jacobi
-----------------------

I implemented the 2D Jacobi smoother on the domain (0,1)x(0,1) with MPI, provided that the number of processors
is equal to a power of four. (This condition is checked and an error thrown if it fails to be true.) Each block
deals with a grid of NlxNl unknowns, not including halo cells. To deal with halo cells, rather than extend each
domain I chose to allocate a 4xNl storage array for each processor to hold halo cells, as well as another
4xNl array to hold its neighbors' halo cells. This simplified the send and receive buffers, maintaining memory
adjacency. The code can be called with two command line arguments, Nl and maxiter. For example,

$ mpirun -np 4 jacobi2D-mpi 37 5000

runs the Jacobi smoother on four blocks, each 37x37 in size, for 5000 iterations. Output will, including timing,
will be written to the file jacobi_np4_Nl37.txt. 

I did a weak scaling study, keeping Nl fixed at 100 while increasing the number of processors
from 1 to 4 to 16 to 64. Each run had 50000 iterations.

Processors   Time (s)
1            2.131954
4            2.490835
16           2.766473
64           3.009821

The running time did increase slowly with the problem size, so the problem doesn't display
perfect weak scaling. The scaling is roughly logarithmic, with time increasing ~0.3 seconds
when the number of processors increases 4-fold. This is most likely due to the increasing message-
passing overhead.

I also did a strong scaling study by fixing the product of (number of processors) and Nl^2, 
halving Nl while increasing the processors four-fold. I was not able to jam in the maximum 
CPU memory and still get a job approved on Prince, so I let Nl max out at 2048. The timings
are presented below. Running time does approximately drop by a factor of 4 each time the processor
count increases by a factor of 4, suggesting that the problem exhibits strong scaling (but not 
weak scaling).. 

Processors    Nl    Time (s)
1             2048   91.088
4             1024   23.210
16            512     6.050
64            256     1.877

--------------------------
       Sample sort
--------------------------

Parallel sample sort is implemented in myssort.cpp. It takes one command line argument, the number N of numbers
per node to sort. For example,

$ mpirun -np 3 myssort 200

will create 200 random integers on each of 3 processors, and sort all 600 in a distributed manner. Processor 0
then writes its results to a file ssort_np3_N200_proc0_sorted.txt, and other other processors similarly. 
The proc0 file also prints the timing at the top. The results from N = 1e4, 1e5, and 1e6 with 64 cores are in the subfolder
ssort_results, and the timings are summarized below. The running time is increasing 

64 cores
N          Time (s)
10000      0.835372
100000     0.847136
1000000    1.508937

