#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <math.h>
#include <stdlib.h>
#include <iomanip>

#define BLOCK_SIZE 1024 // Max threads per block
#define GRID_SIZE 256 

/*
   We need to generalize and make this work even when more blocks than available are requested.
*/

void print_matrix(double *L, int nrows, int ncols){
    //Print out the flattened nrowsxncols matrix L in a formatted way
    for (int i=0; i<nrows; i++){
        for (int j=0; j<ncols; j++){
            //printf("%0.3f    ", L[i*ncols + j]);
            std::cout << std::setprecision(4) << std::setw(10) << L[i + j*nrows] << " ";
        }
        //printf("\n");
        std::cout << std::endl;
    }
}

void matvec_cpu(double* a, double* b, double* c, long m, long n){
    // Multiply the matrix a (mxn) with vector b (nx1) and store the result in c (mx1)
    for (long i=0; i<n; i++){
        c[i] = 0.0;
    }
    for (long j=0; j<n; j++){
        for (long i=0; i<m; i++){
            c[i] += a[i + j*m]*b[j];
        }
    }
}

__global__
void gpu_reduction_for_matvec(double* sumd, double* ad, double* bd, long m, long n, bool multiply){
    __shared__ double smem[BLOCK_SIZE];
    long blocks_per_dot = (n+BLOCK_SIZE-1)/BLOCK_SIZE;
    long i = blockIdx.x / blocks_per_dot; // flat index in c
    long block_along_dot = blockIdx.x - i*blocks_per_dot;
    long j = block_along_dot*BLOCK_SIZE + threadIdx.x;
    if (j < n){
        if (multiply){
            smem[threadIdx.x] = ad[i + m*j]*bd[j];
        } else {
            smem[threadIdx.x] = ad[j + n*i]; // To be used in the latter stages
        }
    } else {
        smem[threadIdx.x] = 0.0;
    }
    __syncthreads();
    // Loop over levels
    for (int s=1; s<BLOCK_SIZE; s*=2){
        int index = 2*s*threadIdx.x;
        if (index < BLOCK_SIZE){
            smem[index] += smem[index+s];
        }
        __syncthreads();
    }
    if (threadIdx.x == 0){
        sumd[blockIdx.x] = smem[threadIdx.x];
    }
}

void matvec_gpu(double* a, double* b, double* c, long m, long n){
    // Compute the dot product with parallel reduction.
    
    // Allocate memory on device
    double *ad, *bd; 
    cudaMalloc((void**) &ad, m*n*sizeof(double));
    cudaMalloc((void**) &bd, n*sizeof(double));
    // Copy memory to device
    cudaMemcpy(ad, a, m*n*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(bd, b, n*sizeof(double), cudaMemcpyHostToDevice);
    cudaDeviceSynchronize();
    
    long N_work = m; 
    for (long i=(n+BLOCK_SIZE-1)/BLOCK_SIZE; i>1; i=(i+BLOCK_SIZE-1)/BLOCK_SIZE) {
        printf("N_work = %ld\n", N_work);
        N_work += m*i;
    }
    printf("N_work = %ld\n", N_work);

    double* yd;
    cudaMalloc((void**) &yd, N_work*sizeof(double));

    double* sumd = yd; //sumd will be a pointer to the current block of memory where to store the current iteration
    long Nb_per_dot = (n+BLOCK_SIZE-1)/BLOCK_SIZE;
    long Niter;

    gpu_reduction_for_matvec<<<Nb_per_dot*m,BLOCK_SIZE>>>(sumd, ad, bd, m, n, true);
    double nothing = 0.0;
    while (Nb_per_dot > 1){
        printf("Begin loop iteration: Nb_per_dot = %d\n", Nb_per_dot);
        Niter = Nb_per_dot*m;
        gpu_reduction_for_matvec<<<Nb_per_dot*m,BLOCK_SIZE>>>(sumd+Niter, sumd, &nothing, m, Nb_per_dot, false);
        Nb_per_dot = (Nb_per_dot+BLOCK_SIZE-1)/BLOCK_SIZE;
        sumd += Niter;
        cudaDeviceSynchronize();
        printf("End loop iteration: Nb_per_dot = %d\n", Nb_per_dot);
    }
    // Copy memory back to the host
    cudaMemcpy(c, sumd, m*sizeof(double), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();
    
    cudaFree(yd);
    cudaFree(ad);
    cudaFree(bd);
}

int main(int argc, char** argv){
    
    // Huge problem: when the matrix size hits 5041x5041, this no longer works.
    // Probably when the blocks are exhausted.


    long n = atol(argv[1]);
    long m = n;
    //long m = 4000;
    //long n = 500;
    //long p = 120;
    srand(time(NULL));
    double* a = (double*) malloc(m*n*sizeof(double));
    double* b = (double*) malloc(n*sizeof(double));
    double* c_cpu = (double*) malloc(m*sizeof(double));
    double* c_gpu = (double*) malloc(m*sizeof(double));
    for (int i=0; i<m*n; i++){
        a[i] = double(rand())/RAND_MAX;
    }   
    for (int i=0; i<n; i++){
        b[i] = double(rand())/RAND_MAX;
    }
    for (int i=0; i<m; i++){
        c_cpu[i] = 0.0;
        c_gpu[i] = 0.0;
    }
    matvec_cpu(a, b, c_cpu, m, n);
    matvec_gpu(a, b, c_gpu, m, n);
    // Print them out
    /*
    printf("a = \n");
    print_matrix(a, m, n);
    printf("b = \n");
    print_matrix(b, 1, n);
    printf("c_cpu = \n");
    print_matrix(c_cpu, 1, m);
    printf("c_gpu = \n");
    print_matrix(c_gpu, 1, m);
    */
    double maxerr = 0.0;
    for (int i=0; i<m; i++){
        maxerr = fmax(maxerr, fabs(c_cpu[i]-c_gpu[i]));
    }
    printf("Max err = %f\n", maxerr);
    free(a);
    free(b);
    free(c_cpu);
    free(c_gpu);

    return 0;
}

