#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <math.h>
#include <stdlib.h>
#include <iomanip>

#define BLOCK_SIZE 1024 // Max threads per block
#define GRID_SIZE 256

/*
   We need to generalize and make this work even when more blocks than available are requested.
*/


void print_matrix(double *L, int nrows, int ncols){
    //Print out the flattened nrowsxncols matrix L in a formatted way
    for (int i=0; i<nrows; i++){
        for (int j=0; j<ncols; j++){
            //printf("%0.3f    ", L[i*ncols + j]);
            std::cout << std::setprecision(4) << std::setw(10) << L[i + j*nrows] << " ";
        }
        //printf("\n");
        std::cout << std::endl;
    }
}

double dotprod_cpu(double* a, double* b, int N){
    double c = 0.0;
    for (int i=0; i<N; i++){
        c += a[i]*b[i];
    }
    return c;
}

void matmul_cpu(double* a, double* b, double* c, long m, long n, long p){
    // Multiply the matrices a (mxn) and b (nxp) and store the result in c (mxp)
    for (int i=0; i<m; i++){
        for (int j=0; j<p; j++){
            for (int k=0; k<n; k++){
                c[i + j*m] += a[i + k*m]*b[k + j*n];
            }
        }
    }
}

__global__
void parallel_dotprod_for_matmul(double* sumd, double* ad, double* bd, long m, long n, long p){
    __shared__ double smem[BLOCK_SIZE];
    long blocks_per_dot = (n+BLOCK_SIZE-1)/BLOCK_SIZE;
    long i_plus_mj = blockIdx.x / blocks_per_dot; // flat index in c
    long j = i_plus_mj / m;
    long i = i_plus_mj - m*j;
    long block_along_dot = blockIdx.x - i_plus_mj*blocks_per_dot;
    long k = block_along_dot*BLOCK_SIZE + threadIdx.x;
    if (k < n){
        smem[threadIdx.x] = ad[i + m*k]*bd[k + n*j];
    } else {
        smem[threadIdx.x] = 0.0;
    }
    __syncthreads();
    // Loop over levels
    for (int s=1; s<BLOCK_SIZE; s*=2){
        int index = 2*s*threadIdx.x;
        if (index < BLOCK_SIZE){
            smem[index] += smem[index+s];
        }
        __syncthreads();
    }
    if (threadIdx.x == 0){
        sumd[blockIdx.x] = smem[threadIdx.x];
        //if (blockIdx.x == 1){
        //    sumd[blockIdx.x] = double(k);
        //}
    }
}

__global__
void parallel_sum_for_matmul(double* sumd, double* xd, long m, long n, long p){
    // This method just sums up elements in an array. Intended for use in later
    // iterations of dot product
    // n will shrink in later loop operations.
    __shared__ double smem[BLOCK_SIZE];
    long blocks_per_dot = (n+BLOCK_SIZE-1)/BLOCK_SIZE;
    long i_plus_mj = blockIdx.x / blocks_per_dot; // flat index in c
    long j = i_plus_mj / m;
    long i = i_plus_mj - m*j;
    long block_along_dot = blockIdx.x - i_plus_mj*blocks_per_dot;
    long k = block_along_dot*BLOCK_SIZE + threadIdx.x;
    if (k < n){
        smem[threadIdx.x] = xd[k + n*(i + m*j)];
    } else {
        smem[threadIdx.x] = 0.0;
    }
    __syncthreads();
    // Loop over levels
    for (int s=1; s<blockDim.x; s*=2){
        int index = 2*s*threadIdx.x;
        if (index < BLOCK_SIZE)
            smem[index] += smem[index+s];
        __syncthreads();
    }
    if (threadIdx.x == 0) sumd[blockIdx.x] = smem[threadIdx.x];
}

void matmul_gpu(double* a, double* b, double* c, long m, long n, long p){
    // Compute the dot product with parallel reduction.
    
    // Allocate memory on device
    double *ad, *bd; 
    cudaMalloc((void**) &ad, m*n*sizeof(double));
    cudaMalloc((void**) &bd, n*p*sizeof(double));
    // Copy memory to device
    cudaMemcpy(ad, a, m*n*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(bd, b, n*p*sizeof(double), cudaMemcpyHostToDevice);
    cudaDeviceSynchronize();
    
    long N_work = m*p; 
    for (long i=(n+BLOCK_SIZE-1)/BLOCK_SIZE; i>1; i=(i+BLOCK_SIZE-1)/BLOCK_SIZE) {
        printf("N_work = %ld\n", N_work);
        N_work += m*p*i;
    }
    printf("N_work = %ld\n", N_work);

    double* yd;
    cudaMalloc((void**) &yd, N_work*sizeof(double));

    double* sumd = yd; //sumd will be a pointer to the current block of memory where to store the current iteration
    long Nb_per_dot = (n+BLOCK_SIZE-1)/BLOCK_SIZE;
    long Niter;

    parallel_dotprod_for_matmul<<<Nb_per_dot*m*p,BLOCK_SIZE>>>(sumd, ad, bd, m, n, p);
    //cudaMemcpy(yd, sumd, Nb_per_dot*m*p*sizeof(double), cudaMemcpyDeviceToHost);
    //printf("After the dot product: c = \n");
    //print_matrix(c, 1, Nb_per_dot*m*p);
    while (Nb_per_dot > 1){
        printf("Begin loop iteration: Nb_per_dot = %d\n", Nb_per_dot);
        Niter = Nb_per_dot*m*p;
        parallel_sum_for_matmul<<<Nb_per_dot*m*p,BLOCK_SIZE>>>(sumd+Niter, sumd, m, Nb_per_dot, p);
        Nb_per_dot = (Nb_per_dot+BLOCK_SIZE-1)/BLOCK_SIZE;
        sumd += Niter;
        cudaDeviceSynchronize();
        //cudaMemcpy(c, sumd, Nb_per_dot*m*p*sizeof(double), cudaMemcpyDeviceToHost);
        //printf("Inside loop: c = \n");
        //print_matrix(c, 1, Nb_per_dot*m*p);
        printf("End loop iteration: Nb_per_dot = %d\n", Nb_per_dot);
    }
    // Copy memory back to the host
    cudaMemcpy(c, sumd, m*p*sizeof(double), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();
    
    cudaFree(yd);
    cudaFree(ad);
    cudaFree(bd);
}

int main(int argc, char** argv){
    long n = atol(argv[1]);
    long m = n;
    long p = n;
    //long m = 4000;
    //long n = 500;
    //long p = 120;
    srand(time(NULL));
    double* a = (double*) malloc(m*n*sizeof(double));
    double* b = (double*) malloc(n*p*sizeof(double));
    double* c_cpu = (double*) malloc(m*p*sizeof(double));
    double* c_gpu = (double*) malloc(m*p*sizeof(double));
    for (int i=0; i<m*n; i++){
        a[i] = double(rand())/RAND_MAX;
    }   
    for (int i=0; i<n*p; i++){
        b[i] = double(rand())/RAND_MAX;
    }
    for (int i=0; i<m*p; i++){
        c_cpu[i] = 0.0;
        c_gpu[i] = 0.0;
    }
    matmul_cpu(a, b, c_cpu, m, n, p);
    matmul_gpu(a, b, c_gpu, m, n, p);
    // Print them out
    /*
    printf("a = \n");
    print_matrix(a, m, n);
    printf("b = \n");
    print_matrix(b, n, p);
    printf("c_cpu = \n");
    print_matrix(c_cpu, m, p);
    printf("c_gpu = \n");
    print_matrix(c_gpu, m, p);
    */
    double maxerr = 0.0;
    for (int i=0; i<m*p; i++){
        maxerr = fmax(maxerr, fabs(c_cpu[i]-c_gpu[i]));
    }
    printf("Max err = %f\n", maxerr);
    free(a);
    free(b);
    free(c_cpu);
    free(c_gpu);

    return 0;
}

