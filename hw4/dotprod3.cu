#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <math.h>

#define BLOCK_SIZE 1024 // Max threads per block
#define GRID_SIZE 256


__global__
void parallel_dotprod(double* sumd, double* ad, double* bd, long N){
    __shared__ double smem[BLOCK_SIZE];
    int idx = threadIdx.x + blockIdx.x*blockDim.x;
    if (idx < N){
        smem[threadIdx.x] = ad[idx]*bd[idx];
    } else{
        smem[threadIdx.x] = 0.0;
    }
    __syncthreads();
    // Loop over levels
    for (int s=1; s<blockDim.x; s*=2){
        int index = 2*s*threadIdx.x;
        if (index < blockDim.x)
            smem[index] += smem[index+s];
    //    if (threadIdx.x % (2*s) == 0)
    //        smem[threadIdx.x] += smem[threadIdx.x + s];
        __syncthreads();
    }
    if (threadIdx.x == 0) sumd[blockIdx.x] = smem[threadIdx.x];
}

__global__
void parallel_sum(double* sumd, double* xd, long N){
    // This method just sums up elements in an array. Intended for use in later
    // iterations of dot product
    __shared__ double smem[BLOCK_SIZE];
    int idx = threadIdx.x + blockIdx.x*blockDim.x;
    if (idx < N){
        smem[threadIdx.x] = xd[idx];
    } else{
        smem[threadIdx.x] = 0.0;
    }
    __syncthreads();
    // Loop over levels
    for (int s=1; s<blockDim.x; s*=2){
        int index = 2*s*threadIdx.x;
        if (index < blockDim.x)
            smem[index] += smem[index+s];
        __syncthreads();
    }
    if (threadIdx.x == 0) sumd[blockIdx.x] = smem[threadIdx.x];
}

double dotprod_gpu(double* a, double* b, long N){
    // Compute the dot product with parallel reduction.
    
    // Allocate memory on device
    double *ad, *bd; 
    cudaMalloc((void**) &ad, N*sizeof(double));
    cudaMalloc((void**) &bd, N*sizeof(double));

    // Copy memory to device
    cudaMemcpy(ad, a, N*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(bd, b, N*sizeof(double), cudaMemcpyHostToDevice);
    cudaDeviceSynchronize();
    
    //Basically copied
    long N_work = 1; // such that BLOCK_SIZE^N_work = N (approximately)
    for (long i=(N+BLOCK_SIZE-1)/BLOCK_SIZE; i>1; i=(i+BLOCK_SIZE-1)/BLOCK_SIZE) {
        N_work += i;
    }
    double* yd;
    cudaMalloc((void**) &yd, N_work*sizeof(double));

    double sum;
    double* sumd = yd; //sumd will be a pointer to the current block of memory where to store the current iteration
    long Nb = (N+BLOCK_SIZE-1)/BLOCK_SIZE; // Might be greater than the number of blocks available. The big question: does threadIdx.x
    
    parallel_dotprod<<<Nb,BLOCK_SIZE>>>(sumd, ad, bd, N);
    cudaMemcpy(&sum, sumd, sizeof(double), cudaMemcpyDeviceToHost);
    printf("After first round, sum = %f\n", sum);
    while (Nb > 1){
        printf("Nb = %d\n", Nb);
        long Niter = Nb;
        Nb = (Nb+BLOCK_SIZE-1)/BLOCK_SIZE;
        parallel_sum<<<Nb,BLOCK_SIZE>>>(sumd+Niter, sumd, Niter);
        sumd += Niter;
    }

    // Copy memory back to the host
    cudaMemcpy(&sum, sumd, sizeof(double), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();

    cudaFree(ad);
    cudaFree(bd);
    cudaFree(yd);
    
    return sum;
}

double dotprod_cpu(double* a, double* b, int N){
    double c = 0.0;
    for (int i=0; i<N; i++){
        c += a[i]*b[i];
    }
    return c;
}

int main(int argc, char** argv){
    long N = (1UL<<25);
    srand(time(NULL));
    double* a = (double*) malloc(N*sizeof(double));
    double* b = (double*) malloc(N*sizeof(double));
    for (int i=0; i<N; i++){
        a[i] = double(rand())/RAND_MAX;
        b[i] = double(rand())/RAND_MAX;
    }   
    double adotb_cpu = dotprod_cpu(a, b, N);
    printf("a.b cpu = %f\n", adotb_cpu);
    double adotb_gpu = dotprod_gpu(a, b, N);
    printf("a.b gpu 1 = %f\n", adotb_gpu);
    printf("gpu 1 error = %f\n", fabs(adotb_cpu - adotb_gpu));
    return 0;

    free(a);
    free(b);
}




