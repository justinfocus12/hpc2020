#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <math.h>

#define BLOCK_SIZE 1024 // Max threads per block
#define GRID_SIZE 256


__global__
void parallel_reduction_1(double* sumd, double* cd, long N){
    __shared__ double smem[BLOCK_SIZE];
    int idx = threadIdx.x + blockIdx.x*blockDim.x;
    if (idx < N){
        smem[threadIdx.x] = cd[idx];
    } else{
        smem[threadIdx.x] = 0.0;
    }
    __syncthreads();
    // Loop over levels
    for (int s=1; s<blockDim.x; s*=2){
        int index = 2*s*threadIdx.x;
        if (index < blockDim.x)
            smem[index] += smem[index+s];
    //    if (threadIdx.x % (2*s) == 0)
    //        smem[threadIdx.x] += smem[threadIdx.x + s];
        __syncthreads();
    }
    if (threadIdx.x == 0) sumd[blockIdx.x] = smem[threadIdx.x];
}

__global__ 
void elementwise_multiply(double* a, double* b, double* c, long N){
    int stride = blockDim.x*gridDim.x;
    int index = threadIdx.x + blockDim.x*blockIdx.x;
    for (int i=index; i<N; i+=stride){
        c[i] = a[i]*b[i];
    }
}

double dotprod_gpu_1(double* a, double* b, long N){
    // Compute the dot product with parallel reduction.
    
    // Allocate memory on the host for the elementwise product c
    double* c = (double*) malloc(N*sizeof(double));

    // Allocate memory on device
    double *ad, *bd, *cd; 
    cudaMalloc((void**) &ad, N*sizeof(double));
    cudaMalloc((void**) &bd, N*sizeof(double));
    cudaMalloc((void**) &cd, N*sizeof(double));

    // Copy memory to device
    cudaMemcpy(ad, a, N*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(bd, b, N*sizeof(double), cudaMemcpyHostToDevice);
    
    elementwise_multiply<<<GRID_SIZE, BLOCK_SIZE>>>(ad, bd, cd, N);
    cudaDeviceSynchronize();
    //Check that the first entries of cd are right
    cudaMemcpy(c, cd, N*sizeof(double), cudaMemcpyDeviceToHost);
    //for (int i=0; i<10; i++){
    //    printf("a[%d], b[%d], c[%d] = %f, %f, %f\n", i,i,i,a[i],b[i],c[i]);
    //}


    //Now sum up that array with parallel reduction 
    
    //Mimic the rest of the reduction algorithm
    long N_work = 1; // such that BLOCK_SIZE^N_work = N (approximately)
    for (long i=(N+BLOCK_SIZE-1)/BLOCK_SIZE; i>1; i=(i+BLOCK_SIZE-1)/BLOCK_SIZE) {
        N_work += i;
    }
    double* yd;
    cudaMalloc((void**) &yd, N_work*sizeof(double));

    double sum;
    double* sumd = yd; //sumd will be a pointer to the current block of memory where to store the current iteration
    long Nb = (N+BLOCK_SIZE-1)/BLOCK_SIZE;
    
    parallel_reduction_1<<<Nb,BLOCK_SIZE>>>(sumd, cd, N);
    cudaMemcpy(&sum, sumd, sizeof(double), cudaMemcpyDeviceToHost);
    printf("After first round, sum = %f\n", sum);
    while (Nb > 1){
        printf("Nb = %d\n", Nb);
        long Niter = Nb;
        Nb = (Nb+BLOCK_SIZE-1)/BLOCK_SIZE;
        parallel_reduction_1<<<Nb,BLOCK_SIZE>>>(sumd+Niter, sumd, Niter);
        sumd += Niter;
    }

    // Copy memory back to the host
    cudaMemcpy(&sum, sumd, sizeof(double), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();

    cudaFree(bd);
    cudaFree(cd);
    cudaFree(yd);
    free(c);
    
    return sum;
}

double dotprod_gpu_0(double* a, double* b, int N){
    // Compute the dot product in two steps. First, compute an elementwise
    // product. Second, sum all those elements.
    
    // Allocate memory on the host for the elementwise product c
    double* c = (double*) malloc(N*sizeof(double));

    // Allocate memory on device
    double *ad, *bd, *cd; 
    cudaMalloc((void**) &ad, N*sizeof(double));
    cudaMalloc((void**) &bd, N*sizeof(double));
    cudaMalloc((void**) &cd, N*sizeof(double));

    // Copy memory to device
    cudaMemcpy(ad, a, N*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(bd, b, N*sizeof(double), cudaMemcpyHostToDevice);

    //Kall the cernel
    elementwise_multiply<<<GRID_SIZE, BLOCK_SIZE>>>(ad, bd, cd, N);
    cudaDeviceSynchronize();
    // Copy memory back to host
    cudaMemcpy(c, cd, N*sizeof(double), cudaMemcpyDeviceToHost);
    
    // Sum up the elementwise product
    double adotb = 0.0;
    for (int i=0; i<N; i++){
        adotb += c[i];
    }
    
    // Free the memory
    cudaFree(ad);
    cudaFree(bd);
    cudaFree(cd);
    
    free(c);
    return adotb;
}


double dotprod_cpu(double* a, double* b, int N){
    double c = 0.0;
    for (int i=0; i<N; i++){
        c += a[i]*b[i];
    }
    return c;
}

int main(int argc, char** argv){
    long N = (1UL<<25); // Errors happen if N gets up to 2<<30. Ignore for now.
    srand(time(NULL));
    double* a = (double*) malloc(N*sizeof(double));
    double* b = (double*) malloc(N*sizeof(double));
    for (int i=0; i<N; i++){
        a[i] = double(rand())/RAND_MAX;
        b[i] = double(rand())/RAND_MAX;
    }   
    double adotb_cpu = dotprod_cpu(a, b, N);
    printf("a.b cpu = %f\n", adotb_cpu);
    double adotb_gpu_0 = dotprod_gpu_0(a, b, N);
    printf("a.b gpu 0 = %f\n", adotb_gpu_0);
    printf("gpu 0 error = %f\n", fabs(adotb_cpu - adotb_gpu_0));
    double adotb_gpu_1 = dotprod_gpu_1(a, b, N);
    printf("a.b gpu 1 = %f\n", adotb_gpu_1);
    printf("gpu 1 error = %f\n", fabs(adotb_cpu - adotb_gpu_1));
    return 0;

    free(a);
    free(b);
}




