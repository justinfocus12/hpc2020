#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctime>

double dotprod_cpu(double* a, double* b, int N){
    // Serial CPU implementation of dot product
    double c = 0.0;
    for (int i=0; i<N; i++){
        c += a[i]*b[i];
    }
    return c;
}

__global__
void elementwise_multiply(double* a, double* b, double* c, int N){
    // Compute the elementwise product of a and b in a grid-stride loop
    int index = threadIdx.x + blockIdx.x*blockDim.x;
    for (int i=index; i<N; i += stride){
        c[i] = a[i]*b[i];
    }
}

__global__
void dotprod_gpu_1block_1thread(double* a, double* b, double* c, int N){
    // GPU implementation of dot product with one block
    int i0 = threadIdx.x;
    int stride = blockDim.x;
    for (int i=i0; i<N; i+=stride){
        *c += a[i]*b[i];
    }
}

__global__
void reduction_gpu_1level(double* a, double* b, double* ab, int N, int level){
    int stride = pow(2, level);

}

double dotprod_gpu_simple(double* a, double* b, int N){
    double c_gpu;
    
    //Initialize device memory
    double *a_dev, *b_dev;
    double *c_dev; 
    cudaMalloc((void**) &a_dev, N*sizeof(double));
    cudaMalloc((void**) &b_dev, N*sizeof(double));
    cudaMalloc((void**) &c_dev, sizeof(double));  // This feels silly, but...

    // Copy memory to device
    cudaMemcpy(a_dev, a, N*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(b_dev, b, N*sizeof(double), cudaMemcpyHostToDevice);

    //Kall the cernel
    dotprod_gpu_1block_1thread<<<1,1>>>(a_dev, b_dev, c_dev, N);
    cudaDeviceSynchronize();

    // Copy memory to host
    cudaMemcpy(&c_gpu, c_dev, sizeof(double), cudaMemcpyDeviceToHost);

    
    // Free memory
    cudaFree(a_dev);
    cudaFree(b_dev);
    cudaFree(c_dev);
    return c_gpu;
}    

int main(int argc, char** argv){
    // Do a dot product on both the CPU and GPU
    int N = atoi(argv[1]);
    srand(time(NULL));
    //Initialize host memory
    double* a = (double*) malloc(N*sizeof(double));
    double* b = (double*) malloc(N*sizeof(double));
    for (int i=0; i<N; i++){
        a[i] = double(rand())/RAND_MAX;
        b[i] = double(rand())/RAND_MAX;
    }

    double c_cpu = dotprod_cpu(a, b, N);
    double c_gpu = dotprod_gpu_simple(a, b, N);
    
    // Check for error
    double err = fabs(c_gpu - c_cpu);
    printf("c_cpu = %f, c_gpu = %f, Error = %f\n", c_cpu, c_gpu, err);
    
    free(a);
    free(b);
    return 0;
}
