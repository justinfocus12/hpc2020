#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <omp.h>
//#include "utils.h"

#define BLOCK_SIZE 1024
#define GRID_SIZE 256

double compute_residual(double* u, double* f, int n){
    double ressq=0.0,locres=0; //squared residual and local residual
    double h2 = 1.0/pow(n+1,2);
    int n2 = pow(n,2);
    int i,j,k;
    for (k=0; k<n2; k++){
        j = k / n;
        i = k - j*n;
        // Compute the residual of the old solution
        locres = -4*u[k];
        if (i<n-1) locres += u[k+1];
        if (i>0) locres += u[k-1];
        if (j<n-1) locres += u[k+n];
        if (j>0) locres += u[k-n];
        locres *= 1.0/h2;
        locres += f[k];
        ressq += pow(locres,2);
    }
    return sqrt(ressq);
}

void jacobi_iteration(double* u, double* f, int n, int maxiter, int nthreads, double* residual, int num_residuals, bool print_iters){
    double h2 = 1.0/((n+1)*(n+1)); //pow(n+1,2);
    int n2 = n*n;
    double* unew = (double*) malloc(n2 * sizeof(double));
    int iter;
    int res_idx = 0; // which residual we're on 
    for (iter=0; iter<maxiter; iter++){
        #pragma omp parallel for num_threads(nthreads)
        for (int k=0; k<n2; k++){
            int tid = omp_get_thread_num();
            int j = k / n;
            int i = k - j*n;
            unew[k] = h2*f[k];
            if (i<n-1) unew[k] += u[k+1];
            if (i>0) unew[k] += u[k-1];
            if (j<n-1) unew[k] += u[k+n];
            if (j>0) unew[k] += u[k-n];
            unew[k] *= 0.25;
        }
        for (int k=0; k<n2; k++){
            u[k] = unew[k];
        }
        if (print_iters && iter % (maxiter/num_residuals)  == (maxiter/num_residuals-1)){
            residual[res_idx] = compute_residual(u,f,n);
            printf("%8d  %12f\n", iter, residual[res_idx]);
            res_idx++;
        }
    }
    residual[num_residuals-1] = compute_residual(u,f,n);
    free(unew);
}

__global__
void jacobi_update_gpu(double* ud, double* unewd, double* fd, double h2, int n){
    int index = blockIdx.x*blockDim.x + threadIdx.x;
    int stride = blockDim.x * gridDim.x;
    int n2 = n*n;
    for (int k=index; k<n2; k+=stride){
        int j = k / n;
        int i = k - j*n;
        unewd[k] = h2*fd[k];
        if (i < n-1) unewd[k] += ud[k+1];
        if (i > 0) unewd[k] += ud[k-1];
        if (j < n-1) unewd[k] += ud[k+n];
        if (j > 0) unewd[k] += ud[k-n];
        unewd[k] *= 0.25;
    }
}

void jacobi_iteration_gpu(double* u, double* f, int n, int maxiter, int nthreads, double* residual, int num_residuals, bool print_iters){
    double h2 = 1.0/((n+1)*(n+1)); 
    int n2 = n*n;

    // Allocate and copy memory to the device
    double *ud, *unewd, *fd;
    cudaMalloc((void**) &ud, n2*sizeof(double));
    cudaMalloc((void**) &unewd, n2*sizeof(double));
    cudaMalloc((void**) &fd, n2*sizeof(double));
    cudaMemcpy(fd, f, n2*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(ud, u, n2*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(unewd, u, n2*sizeof(double), cudaMemcpyHostToDevice);

    int iter;
    int res_idx = 0; // which residual we're on 
    
    for (iter=0; iter<maxiter; iter++){
        jacobi_update_gpu<<<GRID_SIZE,BLOCK_SIZE>>>(ud, unewd, fd, h2, n);
        cudaDeviceSynchronize();
        cudaMemcpy(ud, unewd, n2*sizeof(double), cudaMemcpyDeviceToDevice);
        cudaDeviceSynchronize();
        // Print out results on certain iterations
        if (print_iters && iter % (maxiter/num_residuals)  == (maxiter/num_residuals-1)){
            cudaMemcpy(u, ud, n2*sizeof(double), cudaMemcpyDeviceToHost);
            residual[res_idx] = compute_residual(u,f,n);
            printf("%8d  %12f\n", iter, residual[res_idx]);
            res_idx++;
        }

    }
    residual[num_residuals-1] = compute_residual(u,f,n);
    cudaMemcpy(u, ud, n2*sizeof(double), cudaMemcpyDeviceToHost);
    // Free the memory
    cudaFree(ud);
    cudaFree(unewd);
    cudaFree(fd);
}

void jacobi_either(int N, int maxiter, int nthreads, int num_residuals, double* time, bool use_gpu, bool print_iters){
    // Set up the arrays
    int n = N-1;
    int n2 = n*n;
    double residual[num_residuals]; 
    double* u = (double*) malloc(n2*sizeof(double));
    double* f = (double*) malloc(n2*sizeof(double));
    for (int i=0; i<n*n; i++){
        u[i] = 0.0;
        f[i] = 1.0;
    }
    double start = omp_get_wtime(); 
    if (use_gpu){
        jacobi_iteration_gpu(u, f, n, maxiter, nthreads, residual, num_residuals, print_iters);
    } else {
        jacobi_iteration(u, f, n, maxiter, nthreads, residual, num_residuals, print_iters);
    }
    *time = omp_get_wtime() - start;
    free(u);
    free(f);
}

int main(int argc, char** argv){
    double time_cpu, time_gpu;
    int N = 10000; //atoi(argv[1]);
    int maxiter = 100;
    int nthreads = 20; 
    int num_residuals = 10;
   
    // Memory operations (GB): 10 for each component of Jacobi update
    // (four reads and four writes, plus one at the beginning and the end)
    long memops = pow(N-1,2) * 10 * maxiter * sizeof(double) / 1e9;
    printf("Jacobi iteration, size %d x %d\n", N, N); 
    printf("------------------------\n         CPU\n------------------------\n");
    printf("Iteration    Residual\n");
    jacobi_either(N, maxiter, nthreads, num_residuals, &time_cpu, false, true);
    printf("Time = %f s, Bandwidth = %f GB/s\n", time_cpu, memops/time_cpu);
    printf("------------------------\n         GPU\n------------------------\n");
    printf("Iteration    Residual\n");
    jacobi_either(N, maxiter, nthreads, num_residuals, &time_gpu, true, true);
    printf("Time = %f s, Bandwidth = %f GB/s\n", time_gpu, memops/time_gpu);
    return 0;   
}
    


