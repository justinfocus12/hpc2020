#include <iostream>
#include <stdio.h>
#include <math.h>

int main(int argc, char** argv){
    int nDevices;
    cudaGetDeviceCount(&nDevices);
    for (int i=0; i<nDevices; i++){
        cudaDeviceProp prop;
        cudaGetDeviceProperties(&prop, i);
        printf("Device number: %d\n", i);
        printf("  Device name: %s\n", prop.name);
        printf("  Memory clock rate (kHz): %d\n", prop.memoryClockRate);
        printf("  Memory bus width (bits): %d\n", prop.memoryBusWidth);
        printf("  Peak memory bandwidth (GB/s): %f\n", 
                2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
        printf("  Max threads per block: %d\n", prop.maxThreadsPerBlock);
        printf("  Warp size: %d\n", prop.warpSize);
        printf("  Max grid size: (%d, %d, %d)\n", prop.maxGridSize[0],
                prop.maxGridSize[1], prop.maxGridSize[2]);
        printf("  Max threads dim: (%d, %d, %d)\n", prop.maxThreadsDim[0],
                prop.maxThreadsDim[1],prop.maxThreadsDim[2]);
    }

    return 0;
}
