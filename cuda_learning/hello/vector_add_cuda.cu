#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 10000000

__global__ void vector_add(float *out, float *a, float *b, int n){
    for (int i=0; i<n; i++){
        out[i] = a[i] + b[i];
    }
}

int main(){
    float *a, *b, *out;
    float *d_a, *d_b, *d_out;
    a = (float*) malloc(sizeof(float)*N);
    b = (float*) malloc(sizeof(float)*N);
    out = (float*) malloc(sizeof(float)*N);

    //Initialize array
    for (int i=0; i<N; i++){
        a[i] = 1.0f; b[i] = 2.0f;
    }
    
    //Allocate device memory for a
    cudaMalloc((void**)&d_a, sizeof(float)*N);
    cudaMalloc((void**)&d_b, sizeof(float)*N);
    cudaMalloc((void**)&d_out, sizeof(float)*N);
    
    //Transfer data from host to device memory
    cudaMemcpy(d_a, a, sizeof(float)*N, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, sizeof(float)*N, cudaMemcpyHostToDevice);
    
    //Main function
    vector_add<<<1,1>>>(d_out, d_a, d_b, N);
    cudaMemcpy(out, d_out, sizeof(float)*N, cudaMemcpyDeviceToHost);
    
    float maxerr = 0.0;
    for (int i=0; i<N; i++){
        maxerr = fmaxf(maxerr, fabs(out[i]-3.0));
    }
    printf("Max error = %f\n", maxerr);
    return 0;
    free(a);
    free(b);
    free(out);
    cudaFree(d_a);
    return 0;
}

