#include <iostream>
#include <math.h>
#include <stdio.h>

__global__ 
void cuda_hello(){
    printf("Hello world from GPU!\n");
}

int main() {
    cuda_hello<<<1,1>>>();
    printf("Hello from host\n");
    return 0;
}
