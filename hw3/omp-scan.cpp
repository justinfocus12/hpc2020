#include <algorithm>
#include <stdio.h>
#include <math.h>
#include <omp.h>

// Scan A array and write result into prefix_sum array;
// use long data type to avoid overflow
void scan_seq(long* prefix_sum, const long* A, long n) {
  if (n == 0) return;
  prefix_sum[0] = A[0]; //0;
  for (long i = 1; i < n; i++) {
    prefix_sum[i] = prefix_sum[i-1] + A[i]; //A[i-1];
  }
}

void scan_omp(long* prefix_sum, const long* A, long n, int nthreads) {
    // TODO: implement multi-threaded OpenMP scan
    int max_threads = (int) std::min((double) omp_get_max_threads(), (double) nthreads);
    //printf("Max threads = %d\n", max_threads);
    // Specify the number of subsections. Ideally, we want the size of each section to be the number of threads.
    max_threads = (int) std::min(double(max_threads), double(n));
    // int num_sections = (int) std::min((double) n, ceil(double(n) / max_threads));
    int num_sections = (int) std::min(double(n), (double) max_threads); //std::min(n, max_threads);
    //printf("Num sections = %d\n", num_sections);
    // Determine starting indices of each section
    int start_indices[num_sections];
    int end_indices[num_sections];
    int max_section_length = int(ceil(double(n) / num_sections));
    int last_section_length = n - max_section_length*(num_sections-1); 
    //printf("max,last = %d, %d\n", max_section_length, last_section_length);
    for (int i=0; i<num_sections; i++){
        start_indices[i] = i*max_section_length;
        end_indices[i] = start_indices[i] + max_section_length - 1;
    }
    end_indices[num_sections-1] = n-1;
    // Print out the starting and ending indices
    //printf("Starts    Ends\n");
    //for (int i=0; i<num_sections; i++){
    //    printf("%10d %10d\n", start_indices[i], end_indices[i]);
    //}
    // Now make a parallel for loop over the sections
    #pragma omp parallel for
    for (int i=0; i<num_sections; i++){
        //if (i==0 && omp_get_thread_num()==0) printf("Number of threads = %d\n", omp_get_num_threads());
        long sumi = 0;
        for (int j=start_indices[i]; j<=end_indices[i]; j++){
            sumi += A[j];
            prefix_sum[j] = sumi;
        }
    }
    // Sum the last entries of each block
    for (int i=1; i<num_sections; i++){
        prefix_sum[end_indices[i]] += prefix_sum[end_indices[i-1]];
    }
    // Now start a team to step through the sections and update using previous sections
    #pragma omp parallel for
    for (int i=1; i<num_sections; i++){
        long increment = prefix_sum[end_indices[i-1]];
        for (int j=start_indices[i]; j<end_indices[i]; j++){
            prefix_sum[j] += increment;
        }
    }
    /*
     * This is another way to parallelize the final incrementing step: 
     * one thread updating the first entry of each chunk, another thread 
     * updating the second entry of each chunk, etc. Requires a special 
     * case for the final chunk, which has a different length.
     * /
    #pragma omp parallel for num_threads(10) //max_section_length)
    for (int j=0; j<max_section_length-1; j++){
        for (int i=1; i<num_sections-1; i++){
            prefix_sum[start_indices[i]+j] += prefix_sum[end_indices[i-1]];
        }
        if (j < last_section_length-1){
            int tid = omp_get_thread_num();
            //printf("Thread %d, j=%d Inside the if statement!\n", tid, j);
            int i = num_sections-1; 
            prefix_sum[start_indices[i]+j] += prefix_sum[end_indices[i-1]];
        }
    }
    */
}

int main(int argc, char** argv) {
  long N = 100000000;
  int nthreads = 20; //atoi(argv[1]);
  long* A = (long*) malloc(N * sizeof(long));
  long* B0 = (long*) malloc(N * sizeof(long));
  long* B1 = (long*) malloc(N * sizeof(long));
  for (long i = 0; i < N; i++) A[i] = 1; //rand();

  double tt = omp_get_wtime();
  scan_seq(B0, A, N);
  printf("sequential-scan = %fs\n", omp_get_wtime() - tt);

  tt = omp_get_wtime();
  scan_omp(B1, A, N, nthreads);
  printf("parallel-scan   = %fs\n", omp_get_wtime() - tt);
  //printf("A     Serial cumsum      Parallel cumsum\n"); 
  //for (long i=0; i<N; i++){
  //    printf("%10ld %15ld %15ld\n", A[i], B0[i], B1[i]);
  //}
  long err = 0;
  for (long i = 0; i < N; i++) err = std::max(err, std::abs(B0[i] - B1[i]));
  printf("error = %ld\n", err);

  free(A);
  free(B0);
  free(B1);
  return 0;
}
