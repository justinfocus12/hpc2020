-------------------------
    Sine vectorization
-------------------------

I added several more terms to the AVX vectorization method of sin4_intrin().
The error was the same as the baseline Taylor approximation, 6.928125e-12, but
with a consistent speedup factor of approximately 2 across a wide range of N (number of terms to evaluate.) Below
is a summary of the speedup with different N, in seconds. All three methods
(reference, Taylor approximation, and vectorized Taylor approximation) scaled
linearly with N. 

N     Reference   Taylor      AVX Intrinsic
1e3   0.0208      0.0032      0.0011
1e4   0.2166      0.0335      0.0098
1e5   2.3498      0.2928      0.0902
1e6   19.8429     2.9688      0.8849
1e7   237.5971    31.7580     16.9491



-----------------------
    Parallel Scan
-----------------------

I parallelized the scan operation of the N-length array prefix_array in two stages. First, I split the array into
a number R of chunks, where R is the maximum number of threads available (or N
if there are more available threads than numbers to compute.) the first R-1
entries have length C=ceil(N/R) and the last section has length N-(R-1)*C. This ensures that the last section is the shortest one. The starting and ending indices of each section are pre-computed and stored in a few shared-memory integer arrays. 
A team of threads is then launched to compute the cumulative sums of each chunk in parallel, which is done with a straightforward parallel for loop. These sums are all stored in-place in the array prefix_sum. To stitch all of these sums together, each entry of each chunk (e.g. the indices r*C:(r+1)*C of the r'th chunk) must be incremented by the sum of all entries preceding r*C. This operation would also be nice to do in parallel. However, this increment number I_r depends on the chunk, and must be computed in order: I_1 = prefix_sum[C-1], I_2 = prefix_sum[C-1] + prefix_sum[2*C-1], ... in other words, a scan operation must be done on the sub-array of final indices of each chunk. Perhaps this could be parallelized recursively in the same way, but I just did this part in serial, as a bridge to the final parallel computation. 
After updating the final indices of each chunk, another parallel for loop of the same structure is launched. This time, the entries in chunk r get incremented by the final entry in the previous block.
This final step has another possible implementation: one thread updates the first entry of each chunk, another thread updates the second entry of each chunk, etc. I experimented with both methods, and found similar results (choosing ten threads with this other method.) My final implementation uses the first option.

For small N, the overhead of spawning threads overwhelms the parallel speedup,
resulting in a 1e5 slowdown. But the parallelized running time increases
very slowly with respect to N, sometimes even decreasing, whereas the
sequential version slows down linearly with N. Around N=1e7, the parallel
benefit kicks in. At N=1e9, the speedup is about 10x. Beyond that, my
processor could not handle the array length. Below are timings for a range of
lengths, in seconds, with 20 threads. I am using an x86_64 node on the
UChicago computing cluster, with 2 threads per core, 14 cores per socket, and
2 sockets for a total of 56 cores. This is the maximum number of OpenMP
threads allowed. 
 
N     Sequential  Parallel
1e2   0.000002    0.012125
1e3   0.000005    0.012268
1e4   0.000048    0.014219
1e5   0.000784    0.013055
1e6   0.005014    0.011304
1e7   0.056643    0.017764
1e8   0.449670    0.083094
1e9   8.899737    1.016086

After this, I measured the timing for various numbers of threads with N=1e8.
The parallel running time decreased steadily up to about 20 threads, achieving
a speedup of ~7. Additional threads beyond this had little noticeable effect.

Threads Sequential Parallel
1       0.463648   0.463648
2       0.529828   0.369411
3       0.542656   0.259718
4       0.513396   0.201428
5       0.554748   0.173576
6       0.491927   0.149007
7       0.543826   0.128576
8       0.492536   0.125586
9       0.488821   0.105818
10      0.520429   0.107528
15      0.519778   0.092337
20      0.558480   0.080875
25      0.592902   0.087356
30      0.483239   0.085206
40      0.510404   0.081810
50      0.542224   0.078442


